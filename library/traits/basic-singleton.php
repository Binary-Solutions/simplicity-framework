<?php

  namespace Simplicity\Library\Traits
  {

    /**
     * Trait SingletonBasic
     *
     * Very basic singleton trait with baked in makeshift constructor.
     * @example $SingletonVariable = SingletonBasic::_construct();
     */
    trait BasicSingleton
    {
      /** @var BasicSingleton $Instance This is the instance of the object using the trait if it has been initialized. */
      protected static $Instance;

      /**
       * SingletonBasic constructor.
       *
       * Disable constructors.
       */
      final public function __construct()
      {}

      /**
       * SingletonBasic clone.
       *
       * Disable the clone method.
       */
      final public function __clone()
      {}

      /**
       * SingletonBasic Sleep
       *
       * Disable the sleep method.
       */
      final public function __sleep()
      {}

      /**
       * GetInstance
       *
       * This returns the Singleton instance.
       */
      public static function getInstance()
      {
        if(!self::$Instance){
          $className = get_called_class();
          if(method_exists($className, "_construct")){
            self::$Instance = self::_construct();
            /** Return the instance. */
            return self::$Instance;
          }
          /** @var object Instance  No construt defined just use new static. */
          self::$Instance = new static;
        }
        return self::$Instance;
      }
    }
  }