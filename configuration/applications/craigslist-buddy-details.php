<?php

  namespace Simplicity\Configuration\Applications
  {
    use Simplicity\Configuration\Objects\WebsiteDetails;
    use CraigslistBuddy\CraigslistBuddy;
    use CraigslistBuddy\Configuration\SimplicityConfiguration;

    class CraigslistBuddyDetails extends WebsiteDetails
    {
      protected $_data = [
        self::HostName            => "craigslist-buddy.com",
        self::ApplicationRoot     => "craigslist-buddy.com",
        self::EntryFile           => "craigslist-buddy.php",
        self::EntryClass          => CraigslistBuddy::class,
        self::ConfigurationFile   => "configuration".DIRECTORY_SEPARATOR."simplicity-configuration.php",
        self::ConfigurationClass  => SimplicityConfiguration::class
      ];
    }
  }