<?php

  namespace Simplicity\Configuration\Objects
  {

    class WebsiteDetails
    {

      public const HostName            = 0;

      public const ApplicationRoot     = 1;

      public const EntryFile           = 2;

      public const EntryClass          = 3;

      public const ConfigurationFile   = 4;

      public const ConfigurationClass  = 5;

      protected const Selectors = [
        "hostName"              => self::HostName,
        "applicationRoot"       => self::ApplicationRoot,
        "entryFile"             => self::EntryFile,
        "entryClass"            => self::EntryClass,
        "configurationFile"     => self::ConfigurationFile
      ];

      protected $_data = [
        self::HostName            => "craigslist-buddy.com",
        self::ApplicationRoot     => "craigslist-buddy.com",
        self::EntryFile           => "craigslist-buddy.php",
        self::EntryClass          => CraigslistBuddy::class,
        self::ConfigurationFile   => "configuration".DIRECTORY_SEPARATOR."simplicity-configuration.php",
        self::ConfigurationClass  => SimplicityConfiguration::class
      ];

      public function get(int $selector): string
      {
        return $this->_data[$selector];
      }

      public function getHostName(): string
      {
        return $this->get(self::HostName);
      }

      public function getApplicationRoot(): string
      {
        return $this->get(self::ApplicationRoot);
      }

      public function getEntryFile(): string
      {
        return $this->get(self::EntryFile);
      }

      public function getEntryClass(): string
      {
        return $this->get(self::EntryClass);
      }

      public function getConfigurationFile(): string
      {
        return $this->get(self::ConfigurationFile);
      }

      public function getConfigurationClass(): string
      {
        return $this->get(self::ConfigurationClass);
      }
    }
  }