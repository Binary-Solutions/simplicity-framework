<?php

  namespace Simplicity\Configuration
  {

    use Simplicity\Configuration\Applications\CraigslistBuddyDetails;

    class Applications
    {
      private static $Instance;

      private static $_data = [
        "craigslist-buddy.com" => CraigslistBuddyDetails::class
      ];

      public static function getDetails(string $selector)
      {
        $detailsClass = self::$_data[$selector];
        return new $detailsClass();
      }

      public static function getInstance(): Applications
      {
        if(!self::$Instance){
          self::$Instance = new static;
        }
        return self::$Instance;
      }
    }
  }