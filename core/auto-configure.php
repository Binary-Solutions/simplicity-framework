<?php

  namespace Simplicity\Core
  {
    /** Include the Locations object we will need this to include all of the other objects correctly. */
    include_once("flags.php");
    include_once("locations.php");
    include_once("applications.php");

    use Simplicity\Components\Atlas\Atlas;
    use Simplicity\Components\Euri\Euri;
    use Simplicity\Components\Euri\Objects\Server;
    use Simplicity\Components\Exceptional\Exceptional;
    use Simplicity\Components\Exceptional\Exceptions\Child\UndefinedIndex;
    use Simplicity\Components\Exceptional\Exceptions\Child\UndefinedOffset;
    use Simplicity\Components\Exceptional\Exceptions\SimpleException;
    use Simplicity\Components\Hermes\Hermes;
    use Simplicity\Core\Exceptions\ApplicationFileMissingException;
    use Simplicity\Core\Exceptions\UndefinedApplicationException;
    use Simplicity\Core\Foundation\FrameworkApplication;
    use Simplicity\Simplicity;
    use Simplicity\Components\Exceptional\Exceptions\Child\IncludeOnce;

    /**
     * Class AutoConfigure
     *
     * Setting up the whole framework making sure everything is done in the right order
     * and registered correctly is a big job by itself, so rather than have the whole
     * framework interface class inundated with setup related methods and data I created
     * this object to handle the job.
     */
    class AutoConfigure
    {
      /** @const      int       StaticConstruct         This is the static construct action key. */
      private const StaticConstruct   = 0;
      /** @const      int       IncludeComponent        This is the include component action code. */
      private const IncludeComponent  = 1;
      /** @const      int       IncludeCore             This is the include core action code. */
      private const IncludeCore       = 2;
      /** @const      int       GetInstance             This is the getInstance action code. */
      private const GetInstance       = 3;
      /** @const      array     Actions                 This is an array collection of all the actions. */
      private const Actions = [
        self::StaticConstruct   => [self::class, "staticConstruct"],
        self::IncludeComponent  => [self::class, "includeComponent"],
        self::IncludeCore       => [self::class, "includeCore"],
        self::GetInstance       => [self::class, "getInstance"]
      ];
      /** @const      array     StageOne                This is an array collection of all the stage one actions. */
      private const RequiredComponents = [
        [self::GetInstance,       [Flags::class]],
        [self::StaticConstruct,   [Locations::class]],
        [self::IncludeComponent,  ["exceptional"]],
        [self::StaticConstruct,   [Exceptional::class]],
        [self::IncludeComponent,  ["atlas"]],
        [self::StaticConstruct,   [Atlas::class]],
        [self::IncludeComponent,  ["hermes"]],
        [self::StaticConstruct,   [Hermes::class]],
        [self::IncludeCore,       ["event-registry.php"]]
      ];

      /**
       * execute
       *
       * This method triggers the configurati9on of the Simplicity framework.
       * @throws IncludeOnce
       */
      public static function execute(?FrameworkApplication &$application = null): void
      {
        /** Execute all the actions in the first stage this should enable Exceptional. */
        self::constructRequired(self::RequiredComponents);
        /** Set the EventRegistry. */
        Simplicity::setEventRegistry(EventRegistry::getInstance());
        /** Set the flags object. */
        Simplicity::setFlags(Flags::getInstance());
        /** Load the application. */
        self::loadApplication($application);
        /** Set the Application object. */
        Simplicity::setApplication($application);
        /** Set the configured flag to true. */
        Simplicity::setFlag(Flags::Configured, true);

      }

      /**
       * constructRequired
       *
       * This method will execute all the actions in the provided actions collection for this stage.
       * @param   array   $actions    Array of actions and arguments that need to be processed.
       */
      private static function constructRequired(array $actions): void
      {
        /** @var array $actionData  This is an array of action data that needs to be completed. */
        foreach($actions as $actionData){
          /** Use the list method to seperate the action and arguments into separate variables. */
          list($action, $arguments) = $actionData;
          /** @var callable $callableAction Get the callable for this action. */
          $callableAction = self::Actions[$action];
          /** call the callable action and pass in the arguments. */
          $callableAction($arguments);
        }
      }

      /**
       * loadApplication
       *
       * This makes sure that the object we were passed is in fact an application object and it is ready for use.
       * @param     null|FrameworkApplication           $application    The application object the auto-configure program was passed.
       * @return    void
       * @throws    ApplicationFileMissingException     The exception thrown when an application file can not be located.
       * @throws    UndefinedApplicationException       This exception is thrown when the application is not defined in the data.
       */
      private static function loadApplication(?FrameworkApplication &$application = null): void
      {
        /** Check if application is null. */
        if(is_null($application)){
          /** Include and construct Euri */
          self::staticConstruct([Euri::class]);
          try {
            /** Setup the Applications object. */
            list($directory, $file, $class, $rootNamespace) = Applications::getApplication(Euri::Server(Server::HttpHost));
          } catch (UndefinedIndex|UndefinedOffset $exception){
            /** Throw the undefined application exception. */
            throw new UndefinedApplicationException(Euri::Server(Server::HttpHost));
          }
          /** Try including the application file. */
          try {
            /** Try and include the Application file. */
            Simplicity::includeOnce(Locations::Application, [$directory . DIRECTORY_SEPARATOR . $file]);
          } catch (IncludeOnce $exception){
            /** Throw an application file missing exception. */
            throw new ApplicationFileMissingException(
              Locations::get(Locations::Application).
              DIRECTORY_SEPARATOR.
              $directory.
              DIRECTORY_SEPARATOR.
              $file
            );
          }
          /** Try and construct the Application object. */
          try {
            /** Set the root application namespace. */
            Atlas::setApplicationNamespaceRoot($rootNamespace, Locations::get(Locations::Application).DIRECTORY_SEPARATOR.$directory);
            /** @var FrameworkApplication $Application */
            $application = new $class();
          } catch (SimpleException $exception){
            echo __CLASS__."::".__METHOD__;
            echo "<pre>".print_r($exception, true)."</pre>";
          }
        }
      }

      /**
       * staticConstruct
       *
       * This method will take an array of arguments. It will then use the list method to pull the class name
       * out of the collection. Once that is done it will use the class name to run the static _construct method.
       * @param     array    $arguments     This will be an array of arguments it will only contain a class name.
       */
      private static function staticConstruct(array $arguments): void
      {
        /** Use the list method to parse out the class name value. */
        list($className) = $arguments;
        /** Run the static construct method. */
        $className::_construct();
      }

      /**
       * getInstance
       *
       * This metjod will take an array of arguments containing one value that will be a class name.
       * it will use the list method to pull the class name out then use that to run the getInstance method.
       * @param     array    $arguments     This will be an array of arguments it will only contain a class name.
       */
      private static function getInstance(array $arugments): void
      {
        /** Use the list method to parse out the class name. */
        list($className) = $arugments;
        /** Run the getInstance method on the provided class name. */
        $className::getInstance();
      }

      /**
       * includeComponent
       *
       * This method will include component base files.
       * @param     array         $arguments    Arguments passed in will contain the component name only.
       * @throws    IncludeOnce
       */
      private static function includeComponent(array $arguments): void
      {
        /** Use the list method to pull out the component name. */
        list($componentName) = $arguments;
        /** @var string $componentName  Force the name to lowercase. */
        $componentName = strtolower($componentName);
        /** Include the component. */
        Simplicity::includeOnce(Locations::Components, [$componentName, $componentName.".php"]);
      }

      /**
       * includeCore
       *
       * This method will include core base files.
       * @param     array         $arguments    Arguments passed in will contain the component name only.
       * @throws    IncludeOnce
       */
      private static function includeCore(array $path): void
      {
        /** Include the component. */
        Simplicity::includeOnce(Locations::Core, $path);
      }
    }
  }