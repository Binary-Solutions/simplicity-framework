<?php

  namespace Simplicity\Core
  {

    use Simplicity\Library\Traits\BasicSingleton;

    /**
     * Class Flags
     *
     * The core Flags object keeps track of variu8s true false status indicators for the framework as
     * well as providing an interface for working with them.
     */
    class Flags
    {
      /** Basic singleton trait. */
      use BasicSingleton;

      /** @const    int     Configured    The flag key that denotes if the framework has been configured. */
      public const Configured = 0;

      /** @var      array   $_collection  The flag collection this holds the current status of all the flags.  */
      private $_collection = [
        /** The configured flag defaults to false as it is not configured until it is? */
        self::Configured => false
      ];

      /**
       * is
       *
       * Return the status of the flag specified by the selector.
       * @param   int   $selector   The selector is the flag key you want the status of for instance the Configuration flag is 0.
       * @return  bool
       */
      public static function is(int $selector): bool
      {
        /** Return the boolean value of the flag denoted by the selector. */
        return self::getInstance()->_collection[$selector];
      }

      /**
       * set
       *
       * This will set the flag for the indicated selector to the provided value.
       * @param   int   $selector   The selector for the flag to set.
       * @param   bool  $value      The new value to set for the flag.
       * @return  void
       */
      public static function set(int $selector, bool $value): void
      {
        /** Set the flag for the provided selector to the boolean value. */
        self::getInstance()->_collection[$selector] = $value;
      }

    }
  }