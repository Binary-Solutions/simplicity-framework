<?php

  namespace Simplicity\Core
  {

    /** Import the SingletonBasic. */
    use Simplicity\Library\Traits\BasicSingleton;

    class EventRegistry
    {
      /*+ Use the SingletonBasic Trait. **/
      use BasicSingleton;
      /** @const      int       StartUp           This is the event type to register a callable for. */
      public const StartUp          = 0;
      /** @const      int       Shutdown          This is the event type to register a callable for. */
      public const Shutdown         = 1;
      /** @var        array     $_collection  This is where we keep all the callables. */
      private $_collection          = [self::StartUp => [], self::Shutdown => []];

      /**
       * execute
       *
       * This will iterate over all of the callables in that types collection and call each of them.
       * @param   int   $type   The event type to execute the callbacks for.
       */
      public function execute(int $type): void
      {
        /** @var callable $callable Iterate over each callable and execute it. */
        foreach($this->_collection[$type] as $callable){
          /** Execute the callable. */
          $callable();
        }
      }

      /**
       * register
       *
       * This method will add a callable to the registry for the supplied type.
       * @param     int       $type       The even we are registering for.
       * @param     callable  $callable   The callable we want registered.
       */
      public function register(int $type, callable $callable): void
      {
        /** Use the type key to access the correct part of the collection and push the callable in. */
        array_push($this->_collection[$type], $callable);
      }

      /**
       * clear
       *
       * This will clear the contents out of one of the collections.
       * @param int $type
       */
      public function clear(int $type): void
      {
        /** Overwrite the previous contents with an empty array. */
        $this->_collection[$type] = [];
      }
    }
  }