<?php


  namespace Simplicity\Core\ApplicationType
  {
    /** Import the FrameworkApplication class. */

    use function get_called_class;
    use Simplicity\Components\Atlas\Atlas;
    use Simplicity\Core\Foundation\FrameworkApplication;
    /** Import the frameworks EventRegistry as SimpleRegistry. */
    use Simplicity\Core\EventRegistry as SimpleRegistry;
    use Simplicity\Core\Locations;


    /**
     * Class WebsiteApplication
     *
     * This is the core object that all website applications must extend.
     */
    class WebsiteApplication extends FrameworkApplication
    {

      public function __construct()
      {

      }

      protected function registerExceptionHandler(callable  $callable): void
      {

      }

      /**
       * registerStartup
       *
       * This will registry a callable method that will get called when the Startup method runs.
       * @param   callable    $callable   The method you want called.
       */
      protected function registerStartup(callable $callable): void
      {
        /** Route the request SimpleRegistry. */
        SimpleRegistry::getInstance()->register(SimpleRegistry::StartUp, $callable);
      }

      /**
       * registerShutdown
       *
       * This will register a callable method that will be called when the ShutDown method happens.
       * @param callable $callable
       */
      protected function registerShutdown(callable $callable): void
      {
        /** Route the request SimpleRegistry. */
        SimpleRegistry::getInstance()->register(SimpleRegistry::Shutdown, $callable);
      }
    }
  }