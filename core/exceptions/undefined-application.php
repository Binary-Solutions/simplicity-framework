<?php

  namespace Simplicity\Core\Exceptions
  {

    /**
     * Class UndefinedApplicationException
     *
     * This is the exception the framework throws if the requested application is not defined.
     */
    class UndefinedApplicationException extends FrameworkException
    {
      /** @const  int       ExceptionCode   The Exception Code. */
      protected const ExceptionCode = 1;
      /** @const  string    MessageString   The message string. */
      protected const MessageString = "%s is not a defined application.";

      /**
       * UndefinedApplicationException constructor.
       *
       * Construct the Undefined Application exception.
       * @param   null|string   $name   The application name that was requested.
       * @param   string        $file   The file name it was requested in.a
       * @param   int           $line   The line number it was requested on.
       */
      public function __construct(string $name, string $file = __FILE__, int $line = __LINE__)
      {
        parent::__construct(sprintf(self::MessageString, $name), null, $file, $line);
      }
    }
  }