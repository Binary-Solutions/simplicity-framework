<?php

  namespace Simplicity\Core\Exceptions
  {
    /** Import the component exception class. */
    use Simplicity\Components\Exceptional\Exceptions\ComponentException;

    /**
     * Class FrameworkException
     *
     * This is the base for all the framework exceptions.
     */
    class FrameworkException extends ComponentException
    {
      /** @const  string   ComponentName */
      protected const ComponentName = "Web Site Builder";
      /** @const  int      ComponentCode */
      protected const ComponentCode = 0;
      /** @const  string    DefaultMessage */
      protected const DefaultMessage = "Default Web Site Builder component exception!";
      /**
       * FrameworkException constructor.
       *
       * Recast the constructor for this purpose.
       * @param null|string   $message    The exception message.
       * @param null          $previous   The previous exception if there was one.
       * @param string        $file       The file name.
       * @param int           $line       The line number.
       */
      public function __construct(?string $message, $previous = null, string $file = __FILE__, int $line = __LINE__){
        parent::__construct($message, $file, $line, $previous);
      }
    }
  }