<?php

  namespace Simplicity\Core\Exceptions
  {

    /**
     * Class ApplicationFileMissingException
     *
     * This is thrown when the application exists in the data but the application file referred to in the data
     * could not be included for some reason.
     */
    class ApplicationFileMissingException extends FrameworkException
    {
      /** @const      int     ExceptionCode   This is the exception Code. */
      protected const ExceptionCode = 2;
      /** @const      int     MessageString   This is the message string. */
      protected const MessageString = "Application File Missing %s";

      /**
       * ApplicationFileMissingException constructor.
       *
       * This is thrown when the application exists in the application data but the application file itself
       * defined in the file can not be included for some reason.
       * @param   string  $fileName   The file name we tried to include.
       * @param   string  $file       The file we tried to include it in.
       * @param   int     $line       The line we tried to include it on.
       */
      public function __construct(string $fileName, string $file = __FILE__, int $line = __LINE__)
      {
        parent::__construct(
          sprintf(self::MessageString, $fileName),
          null,
          $file,
          $line
        );
      }
    }
  }