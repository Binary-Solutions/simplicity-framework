<?php

  namespace Simplicity\Core
  {

    use const DIRECTORY_SEPARATOR;

    class Locations
    {
      public const WebRoot          = 0;
      public const Configuration    = 1;
      public const Core             = 2;
      public const Library          = 3;
      public const Components       = 4;
      public const Application      = 5;
      public const ApplicationRoot  = 6;

      private static $Instance;

      private $_selectors = [
        "webRoot"             => self::WebRoot,
        "Configuration"       => self::Configuration,
        "Core"                => self::Core,
        "Library"             => self::Library,
        "Components"          => self::Components,
        "Application"         => self::Application,
        "ApplicationRoot"     => self::ApplicationRoot
      ];

      private $_data = [
        self::WebRoot         => null,
        self::Configuration   => null,
        self::Core            => null,
        self::Library         => null,
        self::Components      => null,
        self::Application     => null,
        self::ApplicationRoot => null
      ];

      public static function _construct()
      {
        if(!self::$Instance){
          self::$Instance = new static;
          self::set(self::WebRoot,        str_replace(DIRECTORY_SEPARATOR."core","",__DIR__));
          self::set(self::Configuration,  self::get(self::WebRoot).DIRECTORY_SEPARATOR."configuration");
          self::set(self::Core,           self::get(self::WebRoot).DIRECTORY_SEPARATOR."core");
          self::set(self::Library,        self::get(self::WebRoot).DIRECTORY_SEPARATOR."library");
          self::set(self::Components,     self::get(self::WebRoot).DIRECTORY_SEPARATOR."components");
          self::set(self::Application,    self::get(self::WebRoot).DIRECTORY_SEPARATOR."application");
        }
      }

      public static function get($selector)
      {
        $instance = self::getInstance();
        if(!is_int($selector)){
          $selector = $instance->_selectors[$selector];
        }
        return $instance->_data[$selector];
      }

      public static function set($selector, $value)
      {
        $instance = self::getInstance();
        if(!is_int($selector)){
          $selector = $instance->_selectors[$selector];
        }
        $instance->_data[$selector] = $value;
      }

      public static function getInstance(): Locations
      {
        if(!self::$Instance){
          self::_construct();
        }
        return self::$Instance;
      }
    }
  }