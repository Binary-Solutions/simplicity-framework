<?php

  namespace Simplicity\Core
  {


    use CraigslistBuddy\Application;
    use Simplicity\Configuration\Data;
    use Simplicity\Library\Traits\BasicSingleton;

    /**
     * Class Applications
     *
     * This class keeps track of which applications the framework has and the basic information needed to
     * initialize them.
     */
    class Applications
    {
      /** Use the SingletonBasic trait. */
      use BasicSingleton;

      private $Data;

      public static function _construct(): Applications
      {
        if(!self::$Instance){
          self::$Instance = new static;
          self::$Instance->Data = Data::getInstance();
        }
        return self::$Instance;
      }

      public static function getApplication(string $selector)
      {
        return self::getInstance()->Data[$selector];

      }


    }
  }