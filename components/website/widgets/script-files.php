<?php

  namespace Simplicity\Components\Website\Widgets
  {
    /** Import the HtmlWidget class into this namespace. */
    use Simplicity\Components\Website\Foundation\HtmlWidget;
    /** Import the DocumentElement Class into this namespace. */

    use Simplicity\Components\Website\Foundation\HtmlWidgetFoundation;
    use Simplicity\Components\Website\Interfaces\DocumentElement;
    /** Import the ScriptTag class into this namespace. */
    use Simplicity\Components\Website\Tags\ScriptTag;

    class ScriptFilesWidget extends HtmlWidgetFoundation implements DocumentElement
    {
      /** @var    \SplObjectStorage    $ObjectStorage    This is where we keep the ScriptTag elements until render. */
      protected $ObjectStorage;

      public function __construct()
      {
        $this->ObjectStorage = new \SplObjectStorage();
      }

      public function add(string $src): void
      {
        $this->ObjectStorage->attach(new ScriptTag(["type" => "text/javascript", "src" => $src]));
      }


      public function render(): string
      {
        $content = "";
        if($this->ObjectStorage->count() > 0){
          foreach($this->ObjectStorage as $element){
            $content .= $element->render();
          }
        }
        return $content;
      }

      public function __toString()
      {
        return $this->render();
      }
    }
  }