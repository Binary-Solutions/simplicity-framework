<?php

  namespace Simplicity\Components\Website\Widgets
  {
    /** Import the HtmlWidget class into this namespace. */
    use Simplicity\Components\Website\Foundation\HtmlWidget;
    /** Import the DocumentElement Class into this namespace. */

    use Simplicity\Components\Website\Foundation\HtmlWidgetFoundation;
    use Simplicity\Components\Website\Interfaces\DocumentElement;
    /** Import the LinkTag class into this namespace. */
    use Simplicity\Components\Website\Tags\LinkTag;

    /**
     * Class StyleSheetsWidget
     *
     * This is the StyleSheetsWidget.
     */
    class StyleSheetsWidget extends HtmlWidgetFoundation implements DocumentElement
    {
      /** @var    \SplObjectStorage    $ObjectStorage    This is where we keep the LinkTag elements until render. */
      protected $ObjectStorage;

      public function __construct()
      {
        $this->ObjectStorage = new \SplObjectStorage();
      }

      public function add(string $href): void
      {
        $this->ObjectStorage->attach(new LinkTag([
          "rel"   => "stylesheet",
          "type"  => "text/css",
          "href"  => $href
        ]));
      }

      public function render(): string
      {
        $content = "";
        if($this->ObjectStorage->count() > 0){
          foreach($this->ObjectStorage as $element){
            $content .= $element->render();
          }
        }
        return $content;
      }

      public function __toString()
      {
        return $this->render();
      }
    }
  }