<?php

  namespace Simplicity\Components\Website\Widgets
  {

    use Simplicity\Components\Website\Foundation\HtmlWidgetFoundation;
    use Simplicity\Components\Website\Interfaces\DocumentElement;
    use Simplicity\Components\Website\Tags\MetaTag;

    class MetaTagsWidget extends HtmlWidgetFoundation implements DocumentElement
    {
      /** @var    \SplObjectStorage    $ObjectStorage    This is where we keep the ScriptTag elements until render. */
      protected $ObjectStorage;

      public function __construct()
      {
        $this->ObjectStorage = new \SplObjectStorage();
      }

      public function add(array $attributes): void
      {
        $this->ObjectStorage->attach(new MetaTag($attributes));
      }

      public function render(): string
      {
        $content = "";
        if($this->ObjectStorage->count() > 0){
          foreach($this->ObjectStorage as $element){
            $content .= $element->render();
          }
        }
        return $content;
      }

      public function __toString()
      {
        return $this->render();
      }
    }
  }