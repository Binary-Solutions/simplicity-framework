<?php

  namespace Simplicity\Components\Website\Widgets
  {
    /** Import the HtmlWidget class into this namespace. */
    use Simplicity\Components\Website\Core\HtmlWidget;

    /**
     * Class HeaderWidget
     *
     * The header widget.
     */
    class HeaderWidget extends HtmlWidget
    {

      public function __construct()
      {


      }

      public function render()
      {
        return "headers";
      }
    }
  }