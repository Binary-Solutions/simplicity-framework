<?php

  namespace Simplicity\Components\Website\Events
  {

    use Simplicity\Components\Website\Foundation\EventFoundation;
    use Simplicity\Components\Website\Objects\Children;

    /**
     * Class GetEvent
     *
     * This is the GetEvent class. This is the class that must be extended with you are
     * creating a webpage that is going to do output html to the client page.
     */
    class GetEvent extends EventFoundation
    {

      /** @var  Children  $Children This is the Children collection. This will hold all the child DocumentElements. */
      protected $Children;

      /**
       * GetEvent constructor.
       *
       * This is the GetEvent constructor. This will get the active get event ready to use.
       */
      public function __construct()
      {
        /** @var Children Children  Construct the Children Collection. */
        $this->Children = new Children();
      }


      /**
       * render
       *
       * This render method will render the event.
       * @return string
       */
      public function render(): string
      {
        /** We only need to return the result of rendering the children  */
        return $this->Children->render();
      }
    }
  }