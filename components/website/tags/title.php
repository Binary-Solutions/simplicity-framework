<?php

  namespace Simplicity\Components\Website\Tags
  {
    /** Import the TagWithValueOnly class implementation. It will provide all of the functionality. */
    use Simplicity\Components\Website\Implementations\TagWithValueOnly;
    /** Import the DocumentElement interface all HtmlTag objects must implement it. */
    use Simplicity\Components\Website\Interfaces\DocumentElement;

    /**
     * Class TitleTag
     *
     * This is the TitleTag object. This will construct the title tag object for use all of the functionality
     * is provided by the class hierarchy. This only says which tag implementation to use and what
     * the tag name is.
     */
    class TitleTag extends TagWithValueOnly implements DocumentElement
    {
      /**
       * TitleTag constructor.
       *
       * This is the constructor for the title tag element.
       * @param   null|string   $tagValue   The value to initialize the title to.
       */
      public function __construct(?string $tagValue = null)
      {
        parent::__construct("title", $tagValue);
      }

      /**
       * Render
       *
       * This is the render method for the title tag element. We added a render method before the parent
       * render so we can catch the render method and just return an empty string if the title has not been set.
       * @return    string    The title tag html string.
       */
      public function render(): string
      {
        if(!$this->getTagValue()){
          return "";
        }
        return parent::render();
      }
    }
  }