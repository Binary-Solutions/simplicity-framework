<?php

  namespace Simplicity\Components\Website\Tags
    {
      /** Import the TagWithChildrenOnly class. It will provide virtually all the functionality. */
      use Simplicity\Components\Website\Implementations\TagWithChildrenOnly;
      /** Import the DocumentElement interface all HtmlTag objects must implement it. */
      use Simplicity\Components\Website\Interfaces\DocumentElement;

      /**
       * Class HeadTag
       *
       * This is the HeadTag object. This will construct the title tag object for use all of the functionality
       * is provided by the class hierarchy. This only says which tag implementation to use and what
       * the tag name is.
       */
    class HeadTag extends TagWithChildrenOnly implements DocumentElement
    {
      /**
       * HeadTag constructor.
       *
       * This is the constructor for the HeadTag element.
       * @param     null|array    $children     Optional argument containing array of child elements.
       * @return    void
       */
      public function __construct(?array $children = null)
      {
        parent::__construct("head", $children);
      }
    }
  }