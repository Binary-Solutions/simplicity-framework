<?php

  namespace Simplicity\Components\Website\Tags
  {
    /** Import TagWithAttributesOnly class into this namespace. */
    use Simplicity\Components\Website\Implementations\TagWithAttributesOnly;

    /**
     * Class LinkTag
     *
     * This is the link tag HtmlElement.
     */
    class LinkTag extends TagWithAttributesOnly
    {

      /**
       * LinkTag constructor.
       *
       * This is the LinkTag constructor.
       * @param   array   $attributes     Optional argument that can contain a collection of key value pair attributes and the value to set them to.
       * @return  void
       */
      public function __construct(array $attributes = [])
      {
        parent::__construct("link", ["rel", "type", "href"], $attributes);
      }
    }
  }