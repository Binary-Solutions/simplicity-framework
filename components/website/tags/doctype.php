<?php

  namespace Simplicity\Components\Website\Tags
  {
    /** Import the TagOnly class into this namespace. */
    use Simplicity\Components\Website\Implementations\TagOnly;
    /** Import the DocumentElement interface into this namespace. */
    use Simplicity\Components\Website\Interfaces\DocumentElement;

    /**
     * Class HtmlTag
     *
     * This is the HtmlTag object. This will construct the title tag object for use all of the functionality
     * is provided by the class hierarchy. This only says which tag implementation to use and what
     * the tag name is.
     */
    class DocTypeTag extends TagOnly implements DocumentElement
    {
      /**
       * DocTypeTag constructor.
       *
       * This will construct the DocType Html tag object.
       */
      public function __construct()
      {
        parent::__construct("!DOCTYPE html");
      }
    }
  }