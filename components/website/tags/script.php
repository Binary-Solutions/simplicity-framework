<?php

  namespace Simplicity\Components\Website\Tags
  {
    /** Import The TagWithAttributesAndValue class into this namespace. */
    use Simplicity\Components\Website\Implementations\TagWithAttributesAndValue;

    /**
     * Class ScriptTag
     *
     * This is the ScriptTag object.
     */
    class ScriptTag extends TagWithAttributesAndValue
    {
      /**
       * ScriptTag constructor.
       *
       * This is the script tag element constructor.
       * @param     null|string   $tagValue         The value to initially apply to this tag.
       * @param     array|null    $attributes       Optional argument that can contain an array of name value pairs of attributes to set.
       * @return    void
       */
      public function __construct(array $attributes = [], ?string $value = null)
      {
        parent::__construct(
          "script",
          ["async", "charset", "defer", "src", "type"],
          $value,
          $attributes
        );
      }
    }
  }