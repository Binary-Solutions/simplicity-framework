<?php

  namespace Simplicity\Components\Website\Tags
  {
    /** Import the TagWithChildrenAndAttributes class. It will provide virtually all the functionality. */
    use Simplicity\Components\Website\Implementations\TagWithChildrenAndAttributes;
    /** Import the DocumentElement interface all HtmlTag objects must implement it. */
    use Simplicity\Components\Website\Interfaces\DocumentElement;

    /**
     * Class BodyTag
     *
     * This is the BodyTag object. This will construct the title tag object for use all of the functionality
     * is provided by the class hierarchy. This only says which tag implementation to use and what
     * the tag name is.
     */
    class BodyTag extends TagWithChildrenAndAttributes implements DocumentElement
    {

      /**
       * BodyTag constructor.
       *
       * This is the constructor for the BodyTag object.
       * @param       null|array        $children           Optional array collection of children to add.
       * @param       null|array        $attributes         Optional array collection of attribute value to set as NVP's.
       * @return      void
       */
      public function __construct(?array $children = null, ?array $attributes = null)
      {
        parent::__construct(
          "body",
          ["accesskey", "class", "contenteditable", "contextmenu", "data-*", "dir", "draggable", "dropzone", "hidden", "id", "lang", "spellcheck", "style", "tabindex", "title", "translate"],
          $children,
          $attributes
        );
      }
    }
  }