<?php

  namespace Simplicity\Components\Website\Tags
  {

    /** Import the TagWithAttributesOnly class into this namespace. */
    use Simplicity\Components\Website\Implementations\TagWithAttributesOnly;

    /**
     * Class MetaTag
     *
     * This is the MetaTag object.
     */
    class MetaTag extends TagWithAttributesOnly
    {
      /**
       * MetaTag constructor.
       *
       * This is the MetaTag constructor.
       * @param     array         $supportedAttributes      This is an array collection of all the attributes this element supports.
       * @param     array|null    $attributes               Optional argument that can contain an array of name value pairs of attributes to set.
       * @return    void
       */
      public function __construct(?array $attributes = null)
      {
        parent::__construct("meta", ["charset", "content", "http-equiv", "name", "scheme"], $attributes);
      }
    }
  }