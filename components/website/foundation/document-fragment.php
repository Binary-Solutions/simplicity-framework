<?php

  namespace Simplicity\Components\Website\Foundation
  {
    /** Import the ElementChildrenTrait. This will provide all of that functionality without needing to redo the work. */
    use Simplicity\Components\Website\Interfaces\DocumentElement;
    /** Import the ElementShortcutTrait. This will provide all the shortcuts for commonly used elements in the document. */
    use Simplicity\Components\Website\Traits\ElementShortcutTrait;

    /**
     * Class DocumentFragment
     *
     * This is the foundation for the DocumentFragment objects. Think of document fragments just as they
     * sound they are whole sections of a document usualy part of the less viewable portion. A good
     * example of a document fragment is the HtmlHeadFragment. That fragment keeps track of the style sheets,
     * javascript includes, page title, and meta data. By doing this we can encapsulate that functionality
     * in a place it belongs rather than flooding the primary HtmlDoucment object with functionality it
     * should only really be a mediator for.
     */
    class DocumentFragment
    {
      /** Use the ElementShortcutTrait. This will provide the collection of shortcuts to commen elements within the document. */
      use ElementShortcutTrait;
      /** @var    DocumentElement   $ParentTag    The parent tag is basicly the wrapper around the fragment.  */
      protected $ParentTag;

      /**
       * DocumentFragment constructor.
       * @param     DocumentElement     $parentElement    The parent or
       */
      public function __construct(DocumentElement $parentElement)
      {
        $this->ParentTag = $parentElement;
      }

      /**
       * Render
       *
       * This method will render the DocumentFragment to an HTML String.
       * @return    string    The html string that represents this fragment.We were exchanging
       */
      public function render(): string
      {
        $this->ParentTag->render();
      }

      /**
       * To String
       *
       * This method will convert the DocumentFragment to a string and return the string representation.
       * @return    string    The string representation of this DocumentFragment.
       */
      public function __toString()
      {
        return $this->render();
      }
    }
  }