<?php

  namespace Simplicity\Components\Website\Foundation
  {

    /**
     * Class EventFoundation
     *
     * This is the foundation object for all webpage events. This class provides all of the basic functionality
     * for both get and post events that they share.
     */
    class EventFoundation
    {


      /**
       * __toString
       *
       * This is the default to string method for all website events. I am of the opinion that all website events
       * should end in a render or a redirect.
       * @return string
       */
      public function __toString(): string
      {
        /** Return the result of render. */
        return $this->render();
      }
    }
  }