<?php

  namespace Simplicity\Components\Website\Foundation
  {
    /** Bring the ChildIndexTrait into this namespace so all the tags have the child index functionality by default. */

    use Simplicity\Components\Website\Interfaces\DocumentElement;
    use Simplicity\Components\Website\Traits\ChildIndexTrait;

    /**
     * Class HtmlTag
     *
     * This class provides all of the standard functionality that every HtmlTag object uses.
     */
    class HtmlTagFoundation
    {
      /** The ChildIndexTrait provides the properties and methods for interacting with a childIndex. */
      use ChildIndexTrait;

      /** @var    string            $_tagName               This is the string tag name of the tag object extending this. This allows the classes to just build the html tag with no help. */
      protected $_tagName;

      public function __construct(string $tagName)
      {
        $this->setTagName($tagName);
      }

      /**
       * Get Tag Name
       *
       * This will get and return the tag name value.
       * @return  string    The tag name string for this tag.
       */
      protected function getTagName(): string
      {
        return $this->_tagName;
      }

      /**
       * Set Tag Name
       *
       * This will set the tag name for this tag.
       * @param   string    $tagName    The tag name to apply to this object.
       * @return  void
       */
      public function setTagName(string $tagName): void
      {
        $this->_tagName = $tagName;
      }

      /**
       * Render
       *
       * This is a fall back method so i know if for some reason the method is missing.
       * @return    string    The fallback render string.
       */
      public function render(): string
      {
        return "Render Method missing!";
      }


      /**
       * To String
       *
       * This will convert the object to a string and return it.
       * @return    string    The object string representation.
       */
      public function __toString(): string
      {
        /** @var    DocumentElement   $this */
        return $this->render();
      }
    }
  }