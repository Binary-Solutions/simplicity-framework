<?php

  namespace Simplicity\Components\Website\Foundation
  {
    /** Import the ElementChildrenTrait. This will provide all of that functionality without needing to redo the work. */
    use Simplicity\Components\Website\Traits\ElementChildrenTrait;
    /** Import the ElementShortcutTrait. This will provide all the shortcuts for commonly used elements in the document. */
    use Simplicity\Components\Website\Traits\ElementShortcutTrait;

    /**
     * Class HtmlDocument
     *
     * This is the foundation for all HtmlDocuments. This will provide any HtmlPage object extending it
     * with all of the core functionality needed to operate.
     */
    class HtmlDocumentFoundation
    {
      /** Use the ElementShortcutTrait. This will provide the collection of shortcuts to commen elements within the document. */
      use ElementShortcutTrait;
      /** Use the ElementChildrenTrait. This will provide all the methods needed to implement a DocumentElement that has a child collection. */
      use ElementChildrenTrait;

      /**
       * HtmlDocument constructor.
       *
       * This will construct the HtmlDocument object and get all of the supporting components ready to use.
       * @return    void
       */
      public function __construct()
      {
        $this->__elementChildrenTrait();
      }

      /**
       * Render
       *
       * This will render the HtmlDocument.
       * @return    string    The html string.
       */
      public function render(): string
      {
        return $this->renderChildren();
      }

      /**
       * To String
       *
       * This method will convert the object to a string and return that representation.
       * @return      string      The string representation of this object.
       */
      public function __toString(): string
      {
        return $this->render();
      }
    }
  }