<?php

  namespace Simplicity\Components\Website\Traits
  {

    /**
     * Trait ChildIndexTrait
     *
     * This traits provides the functionality for setting and getting the ChildIndex.
     */
    trait ChildIndexTrait
    {
      /** @var    int|null    $_childIndex    This is where we store the child index value it is null until it is set. */
      protected $_childIndex;

      /**
       * Get / Set Child index
       *
       * This is a multi use method for both getting and setting the childIndex value.
       * @param   int|null  $index  The child index to apply.
       * @return  int|null
       */
      public function childIndex(?int $index): ?int
      {
        if(!$this->_childIndex && $index){
          $this->_childIndex = $index;
        }
        return $this->_childIndex;
      }

    }
  }