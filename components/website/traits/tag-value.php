<?php

  namespace Simplicity\Components\Website\Traits
  {

    /**
     * Trait TagValueTrait
     *
     * This is the tag value trait. This will provide the required methods for implementing
     * a tag that has a value attribute.
     */
    trait TagValueTrait
    {
      /** @var    string|null     $_tagValue    This is where we store the tag value. */
      protected $_tagValue;

      /**
       * Set Tag Value
       *
       * This method will set the tag value.
       * @param   null|string   $value    The value to apply to this tag.
       */
      public function setTagValue(?string $value): void
      {
        $this->_tagValue = $value;
      }

      /**
       * Get Tag Value
       *
       * This method will return the tag value.
       * @return  null|string
       */
      public function getTagValue(): ? string
      {
        return $this->_tagValue;
      }
    }
  }