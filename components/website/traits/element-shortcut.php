<?php

  namespace Simplicity\Components\Website\Traits
  {
    /** Import the DocumentElement class. Any object that represents a page element that will be rendered must implement it. */
    use Simplicity\Components\Website\Interfaces\DocumentElement;

    /**
     * Trait ElementShortcutTrait
     *
     * This is the element selector trait this trait provides usefull shortcuts to the more common child elements
     * withing some of the document element objects.
     */
    trait ElementShortcutTrait
    {
      /**
       * @var    array     $_elements    This array will be a collection of shortcuts to individual document elements
       * rather than going to find one we use frequently in the SplObjectStoarge object every time we need it.
       */
      protected $_elements = [];

      /**
       * Get Element
       *
       * This will take a predefined element selector and return a reference to it back.
       * @param   int               $elementSelector    The selector for the element you need access to.
       * @return  DocumentElement   The document element indicated by the element selector.
       */
      public function &getElement(int $elementSelector): DocumentElement
      {
        $element =& $this->_elements[$elementSelector];
        return $element;
      }

      /**
       * Set Element
       *
       * This method will take an element selector and a document element object and add the data to the document
       * elements collection for quick reference.
       * @param     int               $elementSelector    The predefined element selector for the element to set.
       * @param     DocumentElement   $documentElement    The document element object to set as the value for this selector.
       * @return    void
       */
      public function setElement(int $elementSelector, DocumentElement $documentElement)
      {
        $this->_elements[$elementSelector] = $documentElement;
      }
    }
  }