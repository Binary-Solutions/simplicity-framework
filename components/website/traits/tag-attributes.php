<?php

  namespace Simplicity\Components\Website\Traits
  {
    /** Import the TagAttributes collection into this namespace. */
    use Simplicity\Components\Website\Collections\TagAttributesCollection;

    /**
     * Trait TagAttributes
     *
     * This is the tag attributes trait this provides all the methods for interfacing with the tag attributes object.
     */
    trait TagAttributesTrait
    {
      /** @var  null|TagAttributesCollection    $TagAttributes          This is the TagAttributes collection object for this tag. */
      protected $TagAttributes;

      /**
       * TagAttributesTrait constructor.
       *
       * I have implemented this syntax to allow for semi logical nested constructors.
       * @param     array         $supportedAttributes      Array collection of attributes names this element supports.
       * @param     null|array    $attributes               Optional argument that can contain an array of name value pairs of attributes to set.
       * @return    void
       * @uses      TagAttributesCollection()
       */
      public function __tagAttributesTrait(array $supportedAttributes = [], ?array $attributes = null): void
      {
        $this->TagAttributes = new TagAttributesCollection($supportedAttributes, $attributes);
      }

      /**
       * getAttribute
       *
       * This will get the attribute value from the TagAttributes object and return it.
       * @param     string        $attributeName    The name of the attribute to retrieve.
       * @return    null|string   The attribute value keep in mind it can be null.
       * @uses      TagAttributesCollection::get()
       */
      public function getAttribute(string $attributeName): ?string
      {
        /** Forward the request on to the Attributes object its is in charge of this. */
        return $this->TagAttributes->get($attributeName);
      }

      /**
       * setAttribute
       *
       * This method accepts an attribute name and an attribute value. It will then pass
       * this information to the TagAttributes object to keep track of it.
       * @param   string  $attributeName    The name of the attribute you want to set.
       * @param   string  $attributeValue   The value you want to set the attribute to.
       * @return  TagAttributesTrait
       * @uses    TagAttributesCollection::set()
       */
      public function setAttribute(string $attributeName, string $attributeValue): TagAttributesTrait
      {
        $this->TagAttributes->set($attributeName, $attributeValue);
        return $this;
      }

      /**
       * Render Attributes
       *
       * This will return a correctly formatted string with all the of the attribute data for this tag.
       * @return    string    All of the html from rendering all of the attributes.
       * @uses      TagAttributesCollection::render()
       */
      protected function renderAttributes(): string
      {
        return $this->TagAttributes->render();
      }
    }
  }