<?php

  namespace Simplicity\Components\Website\Traits
  {
    /** Import the ElementChildrenCollection class into this namespace. */
    use Simplicity\Components\Website\Collections\ElementChildrenCollection;
    /** Import the DocumentElement class into this namespace. */
    use Simplicity\Components\Website\Interfaces\DocumentElement;

    /**
     * Trait ElementChildrenTrait
     *
     * This is the ElementChildrenTrait. This will provide all the functionality for interfacing
     * with an element that has a child element collection.
     */
    trait ElementChildrenTrait
    {
      /** @var  ElementChildrenCollection   $Children */
      protected $Children;

      /**
       * ElementChildrenTrait constructor.
       *
       * This is a makeshift nested constructor for the object implementing this trait to use
       * to ensure this is setup correctly.
       * @return  void
       * @uses    ElementChildrenCollection()
       */
      public function __elementChildrenTrait(?array $children = null): void
      {
        $this->Children = new ElementChildrenCollection($children);
      }

      /**
       * Append Child
       * @param     DocumentElement       $element    The child element to add to the collection
       * @return    void
       * @uses      ElementChildrenCollection::attach()
       */
      public function appendChild(DocumentElement &$element): void
      {
        $this->Children->attach($element);
      }

      /**
       * Get Child
       *
       * Get a child document element.
       * @param   int                     $selector     The child selector to retrieve.
       * @return  null|DocumentElement    The child document element or null.
       * @uses    ElementChildrenCollection::offsetGet()
       */
      //public function &getChild(int $selector): ?DocumentElement
      //{
        //$child =& $this->Children->offsetGet($selector);
        //return $child;
      //}

      /**
       * Has Child
       *
       * This will test if a child exists with that index.
       * @param   int   $selector   The child element index you want to check for.
       * @return  bool  Does it exist?
       * @uses    ElementChildrenCollection::offsetExists()
       */
      //public function hasChild(int $selector): bool
      //{
        //return $this->Children->offsetExists($selector);
      //}

      /**
       * Render
       *
       * This will render all of the child elements and return a string that contains result
       * of each elements render in order as a single string.
       * @return    string    All of the child elements render return.
       */
      public function renderChildren(): string
      {
        return $this->Children->render();
      }
    }
  }