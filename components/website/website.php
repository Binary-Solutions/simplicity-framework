<?php

  namespace Simplicity\Components\Website
  {


    use Simplicity\Library\Traits\BasicSingleton;

    class Website
    {
      /** Use BasicSingleton Trait. */
      use BasicSingleton;

      /**
       * Website constructor
       *
       * This is the static constructor for Website component interface.
       * @return Website
       */
      public static function _construct(): Website
      {
        if(!self::$Instance){
          /** @var Website Instance */
          self::$Instance = new static;




        }
        return self::$Instance;
      }



      public static function constructDocument(array $instructions, &$Children, &$elements): void
      {

      }


    }
  }