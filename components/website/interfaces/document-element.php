<?php

  namespace Simplicity\Components\Website\Interfaces
  {

    interface DocumentElement
    {




      public function render(): string;
    }
  }