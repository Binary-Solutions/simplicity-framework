<?php

  namespace Simplicity\Components\Website\Implementations
  {
    /** Import the HtmlTag Class into this namespace all HtmlTag implementations must extend it. */
    use Simplicity\Components\Website\Foundation\HtmlTagFoundation;
    /** Import the ElementChildrenTrait. This trait will provide all the Child element functionality. */
    use Simplicity\Components\Website\Traits\ElementChildrenTrait;

    /**
     * Class TagWithChildrenOnly
     *
     * This is the HtmlTag implementation for a tab with the ability to have children.
     */
    class TagWithChildrenOnly extends HtmlTagFoundation
    {
      /** Use the ElementChildrenTrait to avoid reproducing that work. */
      use ElementChildrenTrait;
      /** @const    string    OpenTagString     The opening tag string to use with sprintf. */
      private const OpenTagString = "<%s>";
      /** @const    string    CloseTagString    The closing tag string to use with sprintf. */
      private const CloseTagString = "</%s>";

      /**
       * TagWithChildrenOnly constructor.
       *
       * This is the TagWithChildrenOnly constructor. This will construct your tag with this
       * implementation and get it completely ready for use.
       * @param   string      $tagName      The html tag name.
       * @param   array|null  $children     Optional argument consisting of an array of child elements.
       */
      public function __construct(string $tagName, ?array $children = null)
      {
        parent::__construct($tagName);
        $this->__elementChildrenTrait($children);
      }

      /**
       * Render
       *
       * This will render the whole tag down to a string.
       * @return    string    The HTML string.
       */
      public function render(): string
      {
        return
          sprintf(self::OpenTagString, $this->getTagName()).
          $this->renderChildren().
          sprintf(self::CloseTagString, $this->getTagName());
      }
    }
  }