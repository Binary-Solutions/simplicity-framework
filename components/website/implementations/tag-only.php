<?php

  namespace Simplicity\Components\Website\Implementations
  {
    /** Import the HtmlTag class all Tag implementations must extend it. */
    use Simplicity\Components\Website\Foundation\HtmlTagFoundation;

    /**
     * Class TagOnly
     *
     * This is the Tag only implementation of the html tag foundation. This element can not
     * contain child elements. It does not have a value and it has no attributes.
     */
    class TagOnly extends HtmlTagFoundation
    {
      /** @const    string    TagString     The tag string to use with sprintf. */
      private const TagString = "<%s>";

      /**
       * TagOnly constructor.
       *
       * This will construct the tag object.
       * @param     string        $tagName    The html tag name.
       */
      public function __construct(string $tagName)
      {
        parent::__construct($tagName);
      }

      /**
       * Render
       *
       * This will render the whole tag down to a string.
       * @return    string    The HTML string.
       */
      public function render(): string
      {
        return sprintf(self::TagString, $this->getTagName());
      }
    }
  }