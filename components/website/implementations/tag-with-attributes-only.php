<?php

  namespace Simplicity\Components\Website\Implementations
  {
    /** Import the HtmlTagFoundationClass into this namespace. */
    use Simplicity\Components\Website\Foundation\HtmlTagFoundation;
    /** Import the TagAttributesTrait into this namespace. */
    use Simplicity\Components\Website\Traits\TagAttributesTrait;

    /**
     * Class TagWithAttributesOnly
     *
     * This is the TagWithAttributesOnly HtmlTag implementation.
     */
    class TagWithAttributesOnly extends HtmlTagFoundation
    {
      /** Use the TagAttributesTrait to avoid recreating all those functions. */
      use TagAttributesTrait;
      /** @const    string    OpenTagString     The opening tag string to use with sprintf. */
      protected const TagString = "<%s%s>";

      /**
       * TagWithAttributesOnly constructor.
       *
       * This is the constructor for the Tag implementation that has only settable attributes.
       * @param     string        $tagName                  The html name of this tag.
       * @param     array         $supportedAttributes      This is an array collection of all the attributes this element supports.
       * @param     array|null    $chilldren                Optional argument containing an array collection of children to add to the collection.
       * @param     array|null    $attributes               Optional argument that can contain an array of name value pairs of attributes to set.
       * @return    void
       * @uses      ElementChildrenTrait::__elementChildrenTrait()
       * @uses      TagAttributesTrait::__tagAttributesTrait()
       */
      public function __construct(string $tagName, array $supportedAttributes = [], ?array $attributes = null)
      {
        parent::__construct($tagName);
        $this->__tagAttributesTrait($supportedAttributes, $attributes);
      }

      /**
       * Render
       *
       * This is the render method for the TagWithAttributesOnly HtmlTag implementation.
       * @return    string    The html string to render this tag.
       */
      public function render(): string
      {
        return sprintf(
          self::TagString,
          $this->getTagName(),
          $this->renderAttributes()
        );
      }
    }
  }