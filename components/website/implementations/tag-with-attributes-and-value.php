<?php

  namespace Simplicity\Components\Website\Implementations
  {
    /** Import the HtmlTagFoundation class into this namespace. */
    use Simplicity\Components\Website\Foundation\HtmlTagFoundation;
    /** Import the TagAttributesTrait into this namespace. */
    use Simplicity\Components\Website\Traits\TagAttributesTrait;
    /** Import the TagValueTrait into this namespace. */
    use Simplicity\Components\Website\Traits\TagValueTrait;

    /**
     * Class TagWithAttributesAndValue
     *
     * This is the HtmlTag implementation that has both a value and modifiable attributes.
     */
    class TagWithAttributesAndValue extends HtmlTagFoundation
    {
      /** Use the TagAttributesTrait to avoid recreating all those functions. */
      use TagAttributesTrait;
      /** Use the tag value trait to get the tag value functionality. */
      use TagValueTrait;
      /** @const    string    OpenTagString     The opening tag string to use with sprintf. */
      private const OpenTagString = "<%s%s>";
      /** @const    string    CloseTagString    The closing tag string to use with sprintf. */
      private const CloseTagString = "</%s>";

      /**
       * TagWithAttributesAndValue constructor.
       * @param     string        $tagName                  The html name of this tag.
       * @param     array         $supportedAttributes      This is an array collection of all the attributes this element supports.
       * @param     null|string   $tagValue                 The value to initially apply to this tag.
       * @param     array|null    $attributes               Optional argument that can contain an array of name value pairs of attributes to set.
       * @return    void
       */
      public function __construct(string $tagName, array $supportedAttributes = [], ?string $tagValue = null, ?array $attributes = null)
      {
        parent::__construct($tagName);
        $this->setTagValue($tagValue);
        $this->__tagAttributesTrait($supportedAttributes, $attributes);
      }

      /**
       * Render
       *
       * This will render the whole tag down to a string.
       * @return    string    The HTML string.
       */
      public function render(): string
      {
        return
          sprintf(
            self::OpenTagString,
            $this->getTagName(),
            $this->renderAttributes()
          ).
          $this->getTagValue().
          sprintf(
            self::CloseTagString,
            $this->getTagName()
          );
      }
    }
  }