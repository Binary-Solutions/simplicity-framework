<?php

  namespace Simplicity\Components\Website\Implementations
  {
    /** Import the HtmlTag class all Tag implementations must extend it. */
    use Simplicity\Components\Website\Foundation\HtmlTagFoundation;
    /** Import the TagValueTrait so we dont need to reproduce that work needlessly. */
    use Simplicity\Components\Website\Traits\TagValueTrait;

    /**
     * Class TagWithValueOnly
     *
     * This is the Tag with only value not attributes and no children implementation.
     */
    class TagWithValueOnly extends HtmlTagFoundation
    {
      /** Use the tag value trait to get the tag value functionality. */
      use TagValueTrait;
      /** @const    string    OpenTagString     The opening tag string to use with sprintf. */
      private const OpenTagString = "<%s>";
      /** @const    string    CloseTagString    The closing tag string to use with sprintf. */
      private const CloseTagString = "</%s>";

      /**
       * TagWithValueOnly constructor.
       *
       * This will construct the tag object.
       * @param     string        $tagName    The html tag name.
       * @param     null|string   $tagValue   The value to initially apply to this tag.
       */
      public function __construct(string $tagName, ?string $tagValue)
      {
        parent::__construct($tagName);
        $this->setTagValue($tagValue);
      }

      /**
       * Render
       *
       * This will render the whole tag down to a string.
       * @return    string    The HTML string.
       */
      public function render(): string
      {
        return
          sprintf(self::OpenTagString, $this->getTagName()).
          $this->getTagValue().
          sprintf(self::CloseTagString, $this->getTagName());
      }
    }
  }