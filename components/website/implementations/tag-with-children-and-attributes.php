<?php

  namespace Simplicity\Components\Website\Implementations
  {
    /** Import the HtmlTagFoundation Class into this namespace all HtmlTag implementations must extend it. */
    use Simplicity\Components\Website\Foundation\HtmlTagFoundation;
    /** Import the ElementChildrenTrait. This trait will provide all the Child element functionality. */
    use Simplicity\Components\Website\Traits\ElementChildrenTrait;
    /** Import the TagAttributesTrait. This trait will provide all the Attribute functionality. */
    use Simplicity\Components\Website\Traits\TagAttributesTrait;
    /** Import the ElementChildrenCollection class. */
    use Simplicity\Components\Website\Collections\ElementChildrenCollection;
    /** Imp ort the TagAttributesCollection class. */
    use Simplicity\Components\Website\Collections\TagAttributesCollection;

    /**
     * Class TagWithChildrenAndAttributes
     *
     * This is the Html Tag Implementation that has both Child elements and attributes.
     */
    class TagWithChildrenAndAttributes extends HtmlTagFoundation
    {
      /** Use the ElementChildrenTrait to avoid reproducing that work. */
      use ElementChildrenTrait;
      /** Use the TagAttributesTrait to avoid recreating all those functions. */
      use TagAttributesTrait;
      /** @const    string    OpenTagString     The opening tag string to use with sprintf. */
      private const OpenTagString = "<%s%s>";
      /** @const    string    CloseTagString    The closing tag string to use with sprintf. */
      private const CloseTagString = "</%s>";

      /**
       * TagWithChildrenAndAttributes constructor.
       *
       * This is the constructor for the Tag implementation that has both child elements and settable attributes.
       * @param     string        $tagName                  The html name of thbis tag
       * @param     array         $supportedAttributes      This is an array collection of all the attributes this element supports.
       * @param     array|null    $chilldren                Optional argument containing an array collection of children to add to the collection.
       * @param     array|null    $attributes               Optional argument that can contain an array of name value pairs of attributes to set.
       * @return    void
       * @uses      ElementChildrenTrait::__elementChildrenTrait()
       * @uses      TagAttributesTrait::__tagAttributesTrait()
       */
      public function __construct(string $tagName, array $supportedAttributes = [], ?array $children = null, ?array $attributes = null)
      {
        parent::__construct($tagName);
        $this->__elementChildrenTrait($children);
        $this->__tagAttributesTrait($supportedAttributes, $attributes);
      }

      /**
       * Render
       *
       * This will render the whole tag down to a string.
       * @return    string    The HTML string.
       * @uses      TagAttributesCollection::render()
       * @uses      ElementChildrenCollection::render()
       */
      public function render(): string
      {
        return
          sprintf(
            self::OpenTagString,
            $this->getTagName(),
            $this->renderAttributes()
          ).
          $this->renderChildren().
          sprintf(
            self::CloseTagString,
            $this->getTagName()
          );
      }
    }
  }