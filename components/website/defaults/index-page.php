<?php

  namespace Simplicity\Components\Website\Defaults
  {
    /** Import the HtmlDocument class into this namespace. */
    use Simplicity\Components\Website\Foundation\HtmlDocumentFoundation;
    /** Imporet the PageBuilder class into this namespace so it can do the work. */

    use Simplicity\Components\Website\Fragments\HtmlHeadFragment;
    use Simplicity\Components\Website\PageBuilder;
    /** Import the tqg classes this page will need to use.  */
    use Simplicity\Components\Website\Tags\{DoctypeTag, HtmlTag, HeadTag, BodyTag};

    /**
     * Class IndexPage
     *
     * This is a the default index page. When an index page is not provided by a WebsiteApplication
     * and one has been requested this is what will be used.
     */
    class IndexPage extends HtmlDocumentFoundation
    {
      /** @const    int       HtmlDocTypeTag          This is the Element key for the HtmlDocTypeTag. */
      protected const HtmlDocTypeTag        = 0;
      /** @const    int       HtmlTag                 This is the Element key for the HtmlTag. */
      protected const HtmlTag               = 1;
      /** @const    int       HtmlHeadTag             This is the Element key for the HtmlHeadTag. */
      protected const HtmlHeadTag           = 2;
      /** @const    int       HtmlBodyTag             This is the Element key for the HtmlBodyTag. */
      protected const HtmlBodyTag           = 3;

      /** @var    array       $_elements              This is where we keep a reference to each of the elements. */
      protected $_elements                  = [
        self::HtmlDocTypeTag                => null,
        self::HtmlTag                       => null,
        self::HtmlHeadTag                   => null,
        self::HtmlBodyTag                   => null
      ];

      /**
       * IndexPage constructor.
       *
       * This is the constructor for the default Index.
       */
      public function __construct()
      {
        /** The parent construct must be called first. */
        parent::__construct();
        /** Pass the bas construct instructions. */
        PageBuilder::constructPage(
          $this,
          [
            [PageBuilder::ConstructElement, [self::HtmlDocTypeTag,  DocTypeTag::class]],
            [PageBuilder::ConstructElement, [self::HtmlTag,         HtmlTag::class]],
            [PageBuilder::ConstructElement, [self::HtmlHeadTag,     HtmlHeadFragment::class]],
            [PageBuilder::ConstructElement, [self::HtmlBodyTag,     BodyTag::class]],
            [PageBuilder::AppendToDocument, [self::HtmlDocTypeTag]],
            [PageBuilder::AppendToDocument, [self::HtmlTag]],
            [PageBuilder::AppendToElement,  [self::HtmlHeadTag,     self::HtmlTag]],
            [PageBuilder::AppendToElement,  [self::HtmlBodyTag,     self::HtmlTag]]
          ]
        );
      }
    }
  }