<?php

  namespace Simplicity\Components\Website\Fragments
  {
    /** Import the DocumentFragment class into this namespace. */
    use Simplicity\Components\Website\Foundation\DocumentFragment;
    /** Import the DocumentElement class into this namespace. */

    use Simplicity\Components\Website\Foundation\HtmlWidgetFoundation;
    use Simplicity\Components\Website\Interfaces\DocumentElement;
    /** Import the HeadTag class into this namespace. */
    use Simplicity\Components\Website\Tags\HeadTag;
    use Simplicity\Components\Website\Tags\TitleTag;
    use Simplicity\Components\Website\Widgets\MetaTagsWidget;
    use Simplicity\Components\Website\Widgets\ScriptFilesWidget;
    use Simplicity\Components\Website\Widgets\StyleSheetsWidget;

    /**
     * Class HtmlHeadFragment
     *
     * This is the HtmlHeadFragment. This will provide all of the functionality needed to manage everything in the
     * head section of an html document.
     */
    class HtmlHeadFragment extends DocumentFragment implements DocumentElement
    {
      /** @const      int     TitleTag        This is the TitleTag documentElement it only is rendered if the value is set.. */
      private const TitleTag     = 0;
      /** @const      int     MetaTags        This is the Meta tags Collection. It keeps track of all the Meta tag elements until render. */
      private const MetaTags     = 1;
      /** @const      int     StyleSheets     This is the StyleSheetsWidget it keeps track of all the style sheets. */
      private const StyleSheets  = 2;
      /** @const      int     ScriptFiles     This is the ScriptFilesWidget it keeps track of all the script files. */
      private const ScriptFiles  = 3;

      /**
       * HtmlHeadFragment constructor.
       *
       * This is the HtmlHeadFragment constructor.
       */
      public function __construct()
      {
        parent::__construct(new HeadTag());

        $this->setElement(self::TitleTag, new TitleTag());
        $this->ParentTag->appendChild($this->getElement(self::TitleTag));

        $this->setElement(self::MetaTags, new MetaTagsWidget());
        $this->ParentTag->appendChild($this->getElement(self::MetaTags));

        $this->setElement(self::StyleSheets, new StyleSheetsWidget());
        $this->ParentTag->appendChild($this->getElement(self::StyleSheets));

        $this->setElement(self::ScriptFiles, new ScriptFilesWidget());
        $this->ParentTag->appendChild($this->getElement(self::ScriptFiles));

        $this->setTitle("stuff and things.");
        $this->addMetaTag(["charset" => "UTF-8"]);
        $this->addStyleSheet("style-sheet-one.css");
        $this->addStyleSheet("style-sheet-two.css");
        $this->addScriptFile("script-file-one.js");
        $this->addScriptFile("script-file-two.js");

      }


      public function setTitle(string $value): void
      {
        $this->getElement(self::TitleTag)->setTagValue($value);
      }

      public function addMetaTag(array $attributes): void
      {
        $this->getElement(self::MetaTags)->add($attributes);
      }

      public function addStyleSheet(string $src): void
      {
        $this->getElement(self::StyleSheets)->add($src);
      }

      public function addScriptFile(string $href): void
      {
        $this->getElement(self::ScriptFiles)->add($href);
      }


      public function render(): string
      {
        return $this->ParentTag->render();
      }

    }
  }