<?php


  namespace Simplicity\Components\Website
  {

    use Simplicity\Components\Website\Core\HtmlDocument;
    use Simplicity\Components\Website\Foundation\HtmlDocumentFoundation;
    use Simplicity\Components\Website\Interfaces\DocumentElement;

    class PageBuilder
    {



      public const ConstructElement     = 0;

      public const AppendToDocument     = 1;

      public const AppendToElement      = 2;

      public const SetElementAttribute  = 3;


      /** @const      array     MethodCallable      This is a list of callable's associated with each action key. */
      private const MethodCallable = [
        self::ConstructElement        => [self::class, "constructElement"],
        self::AppendToDocument        => [self::class, "appendToDocument"],
        self::AppendToElement         => [self::class, "appendToElement"],
        self::SetElementAttribute     => [self::class, "setElementAttribute"]
      ];


      public static function constructPage(HtmlDocumentFoundation &$page, ?array $instructions): void
      {
        /** Use array map to walk through the array executing the instructions. */
        array_map(
        /** Pass each instruction into the method. */
          function(array $instruction) use(&$page): void
          {
            /** Use the list method to break appart the action key and the method arguments. */
            list($actionKey, $arguments) = $instruction;
            /** @var callable $Callable */
            $Callable = self::MethodCallable[$actionKey];
            /** Use the callable but make sure to pass a reference to the Page object first or we will not be able to do anything. */
            $Callable($page, $arguments);
          },
          /** We are iterating over each one of the build instructions. */
          $instructions
        );
      }

      /**
       * constructElement
       *
       * This method accepts an array argument it must contain the element key you want to construct
       * then the full element class name with namespace.
       * @param   HtmlDocument    $page         The page element we are trying to construct.
       * @param   array           $arguments    [int $elementKey, string $elementClass]
       * @return  void
       */
      protected static function constructElement(HtmlDocumentFoundation &$page, array $arguments): void
      {
        /** Use list to seperate the Element key from the element class name. */
        list($elementKey, $elementClass) = $arguments;
        /** @var DocumentElement $element */
        $element = new $elementClass();
        /** Use the provided data to construct the element and add it to the elements collection. */
        $page->setElement($elementKey, $element);
      }


      /**
       * appendToDocument
       *
       * This method accepts an array argument. It must contain only the element key you want to append to the
       * document INSIDE an array.
       * @param   HtmlDocument    $page         The page element we are trying to construct.
       * @param   array           $arguments    [int $elementKey]
       * @return  void
       */
      protected static function appendToDocument(HtmlDocumentFoundation &$page, array $arguments): void
      {
        /** Use list to pull theElementKey out of the arguments. */
        list($elementKey) = $arguments;

        $element =& $page->getElement($elementKey);

        /** Use the Element key to get the element and add it to the children. */
        $page->appendChild($element);
      }

      /**
       * appendToElement
       *
       * This method takes an array argument it must contain the subject element id and the target
       * element id you want to append to in that order.
       * @param   HtmlDocument    $page         The page element we are trying to construct.
       * @param   array           $arguments    [int $subject, int $target]
       * @return  void
       */
      protected static function appendToElement(HtmlDocumentFoundation &$page, array $arguments): void
      {
        /** Use list to seperate the subject element key from the target element key. */
        list($subject, $target) = $arguments;
        /** Append the subject to the target. */
        $page->getElement($target)->appendChild($page->getElement($subject));
      }

      /**
       * setElementAttribute
       *
       * This takes an array of arguments that must contain the Element key you want to modify then the string
       * selector of the attribute you want to set and lastly the value.
       * @param   HtmlDocumentFoundation    $page         The page element we are trying to construct.Ive told you what
       * @param   array                     $arguments    [int $elementKey, string $selector, string $value]
       * @return  void
       */
      protected static function setElementAttribute(HtmlDocumentFoundation &$page, array $arguments): void
      {
        /** Use the list method to seperate the elementKey, selector and the value. */
        list($elementKey, $selector, $value) = $arguments;
        /** Use t6he element key to get the element and set the attribute selector to the given value. */
        $page->getElement($elementKey)->setAttribute($selector, $value);
      }
    }
  }