<?php

  namespace Simplicity\Components\Website\Collections
  {

    /**
     * Class TagAttributes
     *
     * This is the object that handles tag attributes.
     */
    class TagAttributesCollection
    {
      /** @var  array   $_attributes  Associative array of attributes and their values.  */
      private $_attributes = [];

      /**
       * TagAttributes constructor.
       *
       * This is the constructor for the TagAttributes object.
       * @param   array         $supportedAttributes    Optional list of supported attributes.
       * @param   array|null    $attributes             Optional argument containing an array of attributes to set associated to the values to set them to NVP's.
       */
      public function __construct(array $supportedAttributes = [], ?array $attributes = null)
      {
        /** Use array map to quickly push them all in. */
        array_map([$this, "set"], $supportedAttributes);
        /** Check if attributes were provided we need to set.. */
        if(!is_null($attributes)){
          /** Use array map to quickly iterate over the collection and apply them. */
          array_map([$this, "set"], array_keys($attributes), array_values($attributes));
        }
      }

      /**
       * Get attribute value.
       *
       * This method will get and return the value of an attribute.
       * @param     string          $selector       The attribute selector to get the value of.
       * @return    null|string     The value of the attribute if it has been set otherwise it is initialized to null.
       */
      public function get(string $selector): ?string
      {
        /** Return the value of the attribute. */
        return $this->_attributes[$selector];
      }

      /**
       * Set Attribute value
       *
       * This method will set the value of an attribute.
       * @param     string            $selector     The html a attribute you want to set the value for.
       * @param     null|string       $value        The value you want to apply to the html attribute.
       * @return    TagAttributesCollection
       */
      public function set(string $selector, ?string $value = null): TagAttributesCollection
      {
        /** Set attribute value in the collection. */
        $this->_attributes[$selector] = $value;
        /** return this for method chaining. */
        return $this;
      }

      /**
       * renderAttribute
       *
       * This method takes the attribute name and value and renders the string that
       * will be contained in the tag.
       * @param     string    $selector   The attribute name.
       * @param     string    $value      The value you want the attribute to represent.
       * @return string
       */
      private function renderAttribute($selector, $value): string
      {
        /** Render the attribute string and return it. */
        return "{$selector}=\"{$value}\"";
      }

      /**
       * render
       *
       * This method puts all of the information together and returns a clean html string.
       * @return string
       */
      public function render(): string
      {
        /** @var TagAttributesCollection $instance */
        $instance =& $this;
        /** @var string $content */
        $content = "";
        /** Use array map to walk the collection. */
        array_map(
          /** Pass the attribute name and value into the method. */
          function(string $attributeName, ?string $attributeValue = null) use(&$content, &$instance): void {
            /** Don't print empty attributes. */
            if(!is_null($attributeValue)){
              /** Append the return to the content variable. */
              $content .= " {$instance->renderAttribute($attributeName, $attributeValue)}";
            }
          },
          /** Pass in the attribute names. */
          array_keys($this->_attributes),
          /** Pass in the attribute values. */
          array_values($this->_attributes)
        );
        /** Return the content. */
        return $content;
      }
    }
  }