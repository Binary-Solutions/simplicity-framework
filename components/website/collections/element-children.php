<?php

  namespace Simplicity\Components\Website\Collections
  {
    /** Import the DocumentElement class into this namespace. */
    use Simplicity\Components\Website\Interfaces\DocumentElement;
    use SplObjectStorage;

    /**
     * Class ElementChildrenCollection
     *
     * This is a collection of all the child elements for any HTML entity object.
     */
    class ElementChildrenCollection
    {

      protected $ObjectStorage;

      public function __construct(?array $children = null)
      {
        $this->ObjectStorage = new SplObjectStorage();

        if(!is_null($children)){
          array_map([$this, "attach"], $children);
        }

      }

      public function attach(DocumentElement $element): void
      {
        $this->ObjectStorage->attach($element);
      }


      /**
       * render
       *
       * This is the render method for the children collection. When this is called it will iterate through
       * the whole collection of DocumentElements calling each of their render methods in order and appending
       * the return to the content it will then return to the caller.
       * @return  string    The sum of all the child render methods.
       */
      public function render(): string
      {

        $content = "";

        foreach($this->ObjectStorage as $element){
          $content .= $element->render();
        }

        return $content;
      }
    }
  }