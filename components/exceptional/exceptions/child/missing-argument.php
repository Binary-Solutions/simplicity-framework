<?php

  namespace Simplicity\Components\Exceptional\Exceptions\Child
  {
    use Simplicity\Components\Exceptional\Exceptions\ChildException;
    use Simplicity\Components\Exceptional\Exceptions\Parent\Warning;
    use Simplicity\Components\Exceptional\TestRunner;

    class MissingArgument extends ChildException
    {
      protected static $_parentExceptionClass = Warning::class;

      protected static $_childExceptionCode = 9;

      protected static $_tests = [[TestRunner::StartsWith, "Missing argument"]];
    }
  }