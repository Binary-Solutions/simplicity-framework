<?php

  namespace Simplicity\Components\Exceptional\Exceptions\Child
  {
    use Simplicity\Components\Exceptional\Exceptions\ChildException;
    use Simplicity\Components\Exceptional\Exceptions\Parent\RecoverableError;
    use Simplicity\Components\Exceptional\TestRunner;

    class ObjectToStringConversion extends ChildException
    {
      protected static $_parentExceptionClass = RecoverableError::class;

      protected static $_childExceptionCode = 6;

      protected static $_tests = [[TestRunner::StartsWith, "Object of class "]];
    }
  }