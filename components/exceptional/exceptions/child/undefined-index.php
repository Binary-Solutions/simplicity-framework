<?php

  namespace Simplicity\Components\Exceptional\Exceptions\Child
  {
    use Simplicity\Components\Exceptional\Exceptions\ChildException;
    use Simplicity\Components\Exceptional\Exceptions\Parent\Notice;
    use Simplicity\Components\Exceptional\TestRunner;

    class UndefinedIndex extends ChildException
    {
      protected static $_parentExceptionClass = Notice::class;

      protected static $_childExceptionCode = 3;

      protected static $_tests = [[TestRunner::StartsWith, "Undefined index:"]];
    }
  }