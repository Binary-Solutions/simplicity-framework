<?php

  namespace Simplicity\Components\Exceptional\Exceptions\Child
  {
    use Simplicity\Components\Exceptional\Exceptions\ChildException;
    use Simplicity\Components\Exceptional\Exceptions\Parent\Warning;
    use Simplicity\Components\Exceptional\TestRunner;

    class IncludeOnce extends ChildException
    {
      protected static $_parentExceptionClass = Warning::class;

      protected static $_childExceptionCode = 8;

      protected static $_tests = [[TestRunner::StartsWith, "include_once("]];
    }
  }