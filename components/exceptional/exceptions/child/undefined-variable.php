<?php

  namespace Simplicity\Components\Exceptional\Exceptions\Child
  {
    use Simplicity\Components\Exceptional\Exceptions\ChildException;
    use Simplicity\Components\Exceptional\Exceptions\Parent\Notice;
    use Simplicity\Components\Exceptional\TestRunner;

    class UndefinedVariable extends ChildException
    {

      protected static $_parentExceptionClass = Notice::class;

      protected static $_childExceptionCode = 0;

      protected static $_tests = [[TestRunner::StartsWith, "Undefined variable:"]];

      protected $_variableName;

      public function __construct($message = "", $file = __FILE__, $line = __LINE__, $previous = null)
      {
        $this->setVariableName($message);
        parent::__construct($message, $file, $line, $previous);
      }

      protected function setVariableName(string $name): void
      {
        $this->_variableName = str_replace("Undefined variable: ", null, $name);
      }

      public function variableName(): string
      {
        return $this->_variableName;
      }
    }
  }