<?php

  namespace Simplicity\Components\Exceptional\Exceptions
  {

    use Simplicity\Components\Exceptional\TestRunner;

    class ChildException extends SimpleException
    {
      public const RequiredFields = ["_parentExceptionClass", "_childExceptionCode", "_tests"];

      public function __construct(string $message = "", string $file = __FILE__, int $line = __LINE__, $previous = null){
        parent::__construct($message, self::parentExceptionCode(), self::childExceptionCode(), $file, $line, $previous);
      }

      public static function parentExceptionCode(): int
      {
        $parentClassName = static::$_parentExceptionClass;
        return $parentClassName::exceptionCode();
      }

      public static function childExceptionCode(): int
      {
        return static::$_childExceptionCode;
      }

      public static function test(string $message): bool
      {
        if(is_array(static::$_tests) && sizeof(static::$_tests) >= 1){
          foreach(static::$_tests as $test){
            if(!TestRunner::run($test, $message)){
              return false;
            }
          }
          return true;
        }
        return false;
      }
    }
  }