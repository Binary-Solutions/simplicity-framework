<?php

  namespace Simplicity\Components\Exceptional\Exceptions\Custom
  {

    use Simplicity\Components\Exceptional\Exceptions\ExceptionalException;

    class FailedRegister extends ExceptionalException
    {
      
    }
  }