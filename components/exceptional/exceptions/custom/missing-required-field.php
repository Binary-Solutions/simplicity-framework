<?php

  namespace Simplicity\Core\Exceptional\Exceptions
  {

    use Simplicity\Components\Exceptional\Exceptions\ExceptionalException;

    class MissingRequiredField extends ExceptionalException
    {

      public function __construct(string $exceptionClassName, string $missingFieldName)
      {
        parent::__construct(
          sprintf("Exception: %s missing field - %s.", $exceptionClassName, $missingFieldName));
      }
    }
  }