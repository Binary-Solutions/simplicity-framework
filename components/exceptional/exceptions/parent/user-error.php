<?php

  namespace Simplicity\Components\Exceptional\Exceptions\Parent
  {

    use Simplicity\Components\Exceptional\Exceptions\ParentException;

    class UserError extends ParentException
    {
      protected static $_exceptionCode = E_USER_ERROR;
    }
  }