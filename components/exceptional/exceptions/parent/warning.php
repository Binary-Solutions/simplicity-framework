<?php

  namespace Simplicity\Components\Exceptional\Exceptions\Parent
  {

    use Simplicity\Components\Exceptional\Exceptions\ParentException;

    class Warning extends ParentException
    {
      protected static $_exceptionCode = E_WARNING;
    }
  }