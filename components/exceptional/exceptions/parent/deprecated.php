<?php

  namespace Simplicity\Components\Exceptional\Exceptions\Parent
  {

    use Simplicity\Components\Exceptional\Exceptions\ParentException;

    class Deprecated extends ParentException
    {
      protected static $_exceptionCode = E_DEPRECATED;
    }
  }