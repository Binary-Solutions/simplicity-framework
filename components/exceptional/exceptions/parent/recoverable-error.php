<?php

  namespace Simplicity\Components\Exceptional\Exceptions\Parent
  {

    use Simplicity\Components\Exceptional\Exceptions\ParentException;

    class RecoverableError extends ParentException
    {
      protected static $_exceptionCode = E_RECOVERABLE_ERROR;
    }
  }