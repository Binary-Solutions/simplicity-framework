<?php

  namespace Simplicity\Components\Exceptional\Exceptions\Parent
  {

    use Simplicity\Components\Exceptional\Exceptions\ParentException;

    class CoreWarning extends ParentException
    {
      protected static $_exceptionCode = E_CORE_WARNING;
    }
  }