<?php

  namespace Simplicity\Components\Exceptional\Exceptions\Parent
  {

    use Simplicity\Components\Exceptional\Exceptions\ParentException;

    class CompileWarning extends ParentException
    {
      protected static $_exceptionCode = E_COMPILE_WARNING;
    }
  }