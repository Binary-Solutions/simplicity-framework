<?php

  namespace Simplicity\Components\Exceptional\Exceptions
  {

    class ExceptionalException extends CustomException
    {
      protected static $moduleKey = 1;

      protected static $exceptionCode = 0;

      protected static $defaultMessage = "Exceptional fatal error!";
    }
  }