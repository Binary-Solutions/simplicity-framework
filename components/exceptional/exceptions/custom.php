<?php

  namespace Simplicity\Components\Exceptional\Exceptions
  {

    class CustomException extends SimpleException
    {
      protected $_message;

      protected $_FrameworkCode;

      protected $_ExceptionCode;

      public function __construct(string $message, int $frameworkCode, int $exceptionCode, string $file = __FILE__, int $line = __LINE__, $previous = null)
      {
        $this->setMessage($message)
          ->setFrameworkCode($frameworkCode)
          ->setExceptionCode($exceptionCode);
        parent::__construct(
          $this->message(),
          $this->frameworkCode(),
          $this->exceptionCode(),
          $file,
          $line,
          $previous
        );
      }

      public function message(): string
      {
        return $this->_message;
      }

      public function frameworkCode(): int
      {
        return $this->_FrameworkCode;
      }

      public function exceptionCode(): int
      {
        return $this->_ExceptionCode;
      }

      protected function setMessage(string $message): CustomException
      {
        $this->_message = $message;
        return $this;
      }

      protected function setFrameworkCode(int $code): CustomException
      {
        $this->_FrameworkCode = $code;
        return $this;
      }

      protected function setExceptionCode(int $code): CustomException
      {
        $this->_ExceptionCode = $code;
        return $this;
      }
    }
  }