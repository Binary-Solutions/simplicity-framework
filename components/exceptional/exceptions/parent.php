<?php

  namespace Simplicity\Components\Exceptional\Exceptions
  {

    class ParentException extends SimpleException
    {
      public const RequiredFields = ["_exceptionCode"];

      public function __construct(string $message = "", string $file = __FILE__, int $line = __LINE__, $previous = null){

        parent::__construct(
          $message,
          self::exceptionCode(),
          0,
          $file,
          $line,
          $previous
        );
      }

      public static function exceptionCode()
      {
        return static::$_exceptionCode;
      }
    }
  }