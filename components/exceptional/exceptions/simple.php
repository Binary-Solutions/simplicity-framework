<?php

  namespace Simplicity\Components\Exceptional\Exceptions
  {
    // Use statements
    use Simplicity\Components\Exceptional\Abstracts\SimpleExceptionAbstract;

    class SimpleException extends SimpleExceptionAbstract
    {
      protected $_previous;

      protected $_extra = [];

      public function __construct(string $message = "", int $code = 0, int $severity = 0, string $file = __FILE__,  $line = __LINE__, $previous = null){

        // Handle the $previous being an array.
        if(!is_null($previous)&&is_array($previous)){
          $this->setPrevious($previous);
          $previous = null;
        }
        parent::__construct($message, $code, $severity, $file, $line, $previous);
      }

      protected function setPrevious(array $previous): void
      {
        $this->_previous = $previous;
      }

      protected function setExtra(string $selector, $value)
      {
        if(array_key_exists($selector, $this->_extra)){
          throw new \LogicException(__METHOD__." called on ".__CLASS__." with selector ".$selector.".<br /> This selector is already in use.");
        }
        $this->_extra[$selector] = $value;
      }

      protected function setExtras(array $extras)
      {
        foreach($extras as $selector => $value){
          $this->setExtra($selector, $value);
        }
      }

      public function get(string $selector)
      {
        try {
          switch ($selector) {
            case "message":     return $this->getMessage();   break;
            case "severity":
            case "component":   return $this->getSeverity();  break;
            case "code":        return $this->getCode();      break;
            case "file":        return $this->getFile();      break;
            case "line":        return $this->getLine();      break;
            case "trace":       return $this->getTrace();     break;
            case "previous":    return $this->previous();     break;
            default:
              $result = $this->getExtra($selector);
              if (!is_null($result)) {
                return $result;
              }
              break;
          }
          // Invalid get selector.
          throw new \LogicException(__METHOD__ . " called on " . __CLASS__ . " with selector " . $selector . ".<br />The requested selector is not valid.");
        } catch (\Throwable $exception){

          return "Undefined";
        }
      }

      protected function getExtra(string $selector)
      {
        if(array_key_exists($selector, $this->_extra)){
          return $this->_extra[$selector];
        }
        return null;
      }

      private function previous()
      {
        if(!$this->_previous){
          return $this->getPrevious();
        }
        return $this->_previous;
      }

      // This is the default toString method. This will be a bit ugly since it has to cover everything.
      public function __toString()
      {
        $returnString = "<h1>Default toString method used</h1>";

        if($this instanceof CustomException){
          $returnString .= "<h2>Custom Exception</h2>";
        }

        if($this instanceof ParentException){
          $returnString .= "<h2>Parent Exception</h2>";
        }

        if($this instanceof ChildException){
          $returnString .= "<h2>Child Exception</h2>";
        }

        return $returnString.
          "<table>".
            $this->rowToString("Error Class", __CLASS__).
            $this->rowToString("Message", $this->get("message")).
            $this->rowToString("Severity", $this->get("severity")).
            $this->rowToString("Code", $this->get("code")).
            $this->rowToString("File", $this->get("file")).
            $this->rowToString("Line", $this->get("line")).
            "<tr><td colspan=\"2\">Previous</td></tr>".
            "<tr><td colspan=\"2\"><pre>".print_r($this->get("previous"), true)."</pre></td></tr>".
          "</table>";
      }

      protected function rowToString(string $label, string $value)
      {
        return "<tr><td>$label</td><td>$value</td></tr>";
      }

      protected function extraIsDefined(string $selector): bool
      {
        return array_key_exists($selector, $this->_extra);
      }


      // NOT IMPLEMENTED

      public function serialize()
      {}

      public function unSerialize($serialized)
      {}
    }
  }