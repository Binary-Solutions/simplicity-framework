<?php

  namespace Simplicity\Components\Exceptional\Objects
  {
    use Simplicity\Components\Exceptional\Exceptions\ChildException;
    use Simplicity\Components\Exceptional\Exceptions\Custom\ChildExists;
    use Simplicity\Components\Exceptional\Exceptions\Custom\RegisterType;

    class Entry
    {

      private $ParentClassName;

      private $_children = [];

      private $_hasChildren = false;

      public function __construct(string $exceptionClassName)
      {
        $this->setParentClassName($exceptionClassName);
      }

      public function getChild(int $childCode): string
      {
        return $this->_children[$childCode];
      }

      public function parentClassName(): string
      {
        return $this->ParentClassName;
      }

      public function parentExceptionCode(): int
      {
        return $this->parentClassName()::exceptionCode();
      }

      public function hasChildren(): bool
      {
        return $this->_hasChildren;
      }

      public function childExists(int $key): bool
      {
        return array_key_exists($key, $this->_children);
      }

      public function setParentClassName(string $exceptionClassName): Entry
      {
        $this->ParentClassName = $exceptionClassName;
        return $this;
      }

      private function setHasChildren(bool $value): Entry
      {
        $this->_hasChildren = $value;
        return $this;
      }

      public function register(string $exceptionClassName): Entry
      {
        if(!is_subclass_of($exceptionClassName, ChildException::class)){
          throw new RegisterType("You can only register a ChildException to a ParentException!");
        }
        if($this->childExists($exceptionClassName::childExceptionCode())){
          throw new ChildExists("ChildException with code ".$exceptionClassName::childExceptionCode()." already exists!");
        }
        $this->_children[$exceptionClassName::childExceptionCode()] = $exceptionClassName;
        if(!$this->hasChildren()){
          $this->setHasChildren(true);
        }
        return $this;
      }

      public function bestMatch($message): string
      {
        if($this->hasChildren()){
          foreach($this->_children as $child){
            if($child::test($message)){
              return $child;
            }
          }
        }
        return $this->parentClassName();
      }
    }
  }