<?php

  namespace Simplicity\Components\Exceptional
  {
    /** Import the Simplicity class into this namespace. */
    use Simplicity\Simplicity;
    /** Import the locations class into this namespace. */
    use Simplicity\Core\Locations;
    /** Include the Registry object. We have to do this manually.  */
    Simplicity::includeOnce(Locations::Components, ["exceptional", "registry.php"]);
    /** Use the BasicSingleton Trait. */
    use Simplicity\Library\Traits\BasicSingleton;
    /** Bring the SimpleException class into this namespace. */
    use Simplicity\Components\Exceptional\Exceptions\SimpleException;

    /**
     * Class Exceptional
     *
     * Exceptional is where much of my inspiration for this framework came from. This object represents a change
     * in the way developers write php. This will allow you to avoid those horrid if, else logic chains. Stop worrying
     * about ELSE. you try to do something and you catch only the exception you are concurned with and act on that
     * if it happens if it does not happen php/apache have done no work. If you try the same thing with an if/else
     * block you are creating a split each time you use the word else, and creating more work for apache and
     * more complicated code.
     */
    class Exceptional
    {
      /** Use the basic singleton train to simplify the code. */
      use BasicSingleton;
      /** @var Registry   $Registry  */
      private $Registry;
      /** @var    TestRunner    $TestRunner     This object is responsible for providing all of the tests for recasting php exceptions. */
      private $TestRunner;

      /**
       * Exceptional static constructor
       *
       * This is the constructor for the Exceptional component object.
       * @return  Exceptional
       */
      public static function _construct(): Exceptional
      {
        if(!self::$Instance){
          /** @var Exceptional Instance   Bind the new static exceptional instance. */
          self::$Instance = new static;
          /** Set php's error reporting level. */
          ini_set('error_reporting', E_ALL & ~E_NOTICE);
          /** Set apache to not display errors on the screen. */
          ini_set("display_errors", false);
          /** Set apache to assert all exceptions. */
          ini_set("assert.exceptions", true);
          /** Register Exceptionals handleError method as the error handler rather than php or apache. */
          set_error_handler(array(Exceptional::class, "handleError"));
          /** Register Exceptional's handleException method as the exception handler rather than php or apache. */
          set_exception_handler(array(Exceptional::class, "handleException"));
          /** @var Registry Registry Save a reference to the Registry we will need it. */
          self::$Instance->Registry   = Registry::getInstance();
          /** @var TestRunner TestRunner  Save a reference to the test runner. */
          self::$Instance->TestRunner = TestRunner::initialize();
        }
        // Return the completed Exceptional component.php.
        return self::$Instance;
      }

      /**
       * handleError
       *
       * This method is called by php whenever an error type exception is thrown. As of php 7.0 almost all exceptions
       * get thrown as error exceptions meaning they end up here rather than the orignal exception handler we used
       * in php 5.6.
       * @param   int       $severity     The severity code reported by the exception.
       * @param   string    $message      The message that is being provided by the exception.
       * @param   string    $file         The file the exception occurred in.
       * @param   int       $line         The line number the exception occured on.
       * @param   \Error|\ErrorException|\Exception|\Throwable|\TypeError|array   $context    The context object explains the context this was thrown in 90% of the time it is an exception.
       */
      public static function handleError(int $severity, string $message, string $file, int $line, $context)
      {
        /** @var Exceptional $instance  This is the Instance of exceptional. */
        $instance = self::getInstance();
        /** @var string $exceptionClassName This is the class name of the best type of exception exceptional could recast to. */
        $exceptionClassName = $instance->Registry->bestMatch($severity, $message, $file, $line, $context);
        /** Throw the new exception. */
        throw new $exceptionClassName($message, $file, $line, $context);
      }

      /**
       * handleException
       *
       * This is a relic from the php 5.6 version of Exceptional. The old method was to catch the exceptions
       * then recast them here. As of php 7.0 exceptions are thrown as "Throwable's" / "Error" objects. So
       * they from my current experience get captured in the handle error method. This method may end up
       * getting removed if i can prove to myself that it is not needed in any case. the last thing we need
       * is an exception getting missed that contains sensitive data.
       * @param   \Throwable|\Error|\ErrorException|\TypeError|\Exception     $exception    The exception that was thrown.
       * @return  void
       * @todo    Silence this method before doing any production push.
       */
      public static function handleException($exception): void
      {
        /** Setting up a big header so I know what this is if it happens during development. */
        echo "<h1>Exception caught in ".__CLASS__."::".__Method__." in ".__FILE__." on line ".__LINE__."</h1>";
        echo "<pre>".print_r($exception, true)."</pre>";
        if(!is_subclass_of($exception,SimpleException::class)){
          echo "$" . __CLASS__ . "::" . __FUNCTION__ . "<br />";
          echo "<pre>" . print_r($exception, true) . "</pre>";
        }
        exit();
      }

      /**
       * register
       *
       * This is a pass-through method to expose the register method in the registry allowing
       * custom exceptions to be written and register with Exceptional.
       * @param   string  $exceptionClass   The name of the exception class you are registering.
       * @return  void
       */
      public static function register(string $exceptionClass): void
      {
        /** Pass the request to the Registry. */
        Registry::register($exceptionClass);
      }
    }
  }