<?php

  namespace Simplicity\Components\Exceptional\Interfaces
  {

    interface ThrowableExceptionInterface extends \Throwable
    {}
  }