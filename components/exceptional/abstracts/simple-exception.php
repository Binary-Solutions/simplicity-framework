<?php

  namespace Simplicity\Components\Exceptional\Abstracts
  {

    abstract class SimpleExceptionAbstract extends ThrowableExceptionAbstract implements \Serializable
    {}
  }