<?php

  namespace Simplicity\Components\Exceptional\Abstracts
  {

    use Simplicity\Components\Exceptional\Interfaces\ThrowableExceptionInterface;

    abstract class ThrowableExceptionAbstract extends \ErrorException implements ThrowableExceptionInterface
    {}
  }