<?php

  namespace Simplicity\Components\Exceptional
  {

    class TestRunner
    {

      public const StartsWith = 0;

      public const EndsWith = 1;

      protected static $Instance;

      private $tests = [
        self::StartsWith => null,
        self::EndsWith => null
      ];

      public static function initialize(): TestRunner
      {
        if(!self::$Instance) {
          self::$Instance = new static;
          self::$Instance->tests[self::StartsWith] = "testStartsWith";
          self::$Instance->tests[self::EndsWith] = "testEndsWith";
        }
        return self::$Instance;
      }

      public static function getInstance(): TestRunner
      {
        if(!self::$Instance){
          self::$Instance = self::initialize();
        }
        return self::$Instance;
      }

      public static function run(array $arguments, string $message): bool
      {
        $instance = self::getInstance();
        list($type, $target) = $arguments;
        return call_user_func(array($instance, $instance->tests[$type]), strtoupper($message), strtoupper($target));
      }

      private static function testStartsWith(string $message, string $target): bool
      {
        return (substr($message, 0, strlen($target)) == $target);
      }

      private static function testEndsWith(string $message, string $target): bool
      {
        return (substr($message, (strlen($message)-strlen($target)), strlen($message)) == $target);
      }
    }
  }