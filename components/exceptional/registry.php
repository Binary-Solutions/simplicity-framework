<?php

  namespace Simplicity\Components\Exceptional
  {
    use Simplicity\Simplicity;
    use Simplicity\Core\Locations;
    // Base level components.
    Simplicity::includeOnce(Locations::Components, ["exceptional", "test-runner.php"]);
    // Interfaces
    Simplicity::includeOnce(Locations::Components, ["exceptional", "interfaces", "throwable-exception.php"]);
    // Abstracts
    Simplicity::includeOnce(Locations::Components, ["exceptional", "abstracts", "throwable-exception.php"]);
    Simplicity::includeOnce(Locations::Components, ["exceptional", "abstracts", "simple-exception.php"]);
    // The core exception types
    Simplicity::includeOnce(Locations::Components, ["exceptional", "exceptions", "simple.php"]);
    Simplicity::includeOnce(Locations::Components, ["exceptional", "exceptions", "custom.php"]);
    Simplicity::includeOnce(Locations::Components, ["exceptional", "exceptions", "exceptional.php"]);
    Simplicity::includeOnce(Locations::Components, ["exceptional", "exceptions", "component.php"]);
    // Exceptions specific
    Simplicity::includeOnce(Locations::Components, ["exceptional", "exceptions", "custom", "register-type.php"]);
    Simplicity::includeOnce(Locations::Components, ["exceptional", "exceptions", "custom", "failed-register.php"]);
    Simplicity::includeOnce(Locations::Components, ["exceptional", "exceptions", "custom", "child-exists.php"]);
    Simplicity::includeOnce(Locations::Components, ["exceptional", "exceptions", "custom", "missing-required-field.php"]);
    // Last 2 core exception types.
    Simplicity::includeOnce(Locations::Components, ["exceptional", "exceptions", "parent.php"]);
    Simplicity::includeOnce(Locations::Components, ["exceptional", "exceptions", "child.php"]);
    // Parent Exceptions
    Simplicity::includeOnce(Locations::Components, ["exceptional", "exceptions", "parent", "compile-error.php"]);
    Simplicity::includeOnce(Locations::Components, ["exceptional", "exceptions", "parent", "compile-warning.php"]);
    Simplicity::includeOnce(Locations::Components, ["exceptional", "exceptions", "parent", "core-error.php"]);
    Simplicity::includeOnce(Locations::Components, ["exceptional", "exceptions", "parent", "core-warning.php"]);
    Simplicity::includeOnce(Locations::Components, ["exceptional", "exceptions", "parent", "deprecated.php"]);
    Simplicity::includeOnce(Locations::Components, ["exceptional", "exceptions", "parent", "error.php"]);
    Simplicity::includeOnce(Locations::Components, ["exceptional", "exceptions", "parent", "notice.php"]);
    Simplicity::includeOnce(Locations::Components, ["exceptional", "exceptions", "parent", "parse.php"]);
    Simplicity::includeOnce(Locations::Components, ["exceptional", "exceptions", "parent", "recoverable-error.php"]);
    Simplicity::includeOnce(Locations::Components, ["exceptional", "exceptions", "parent", "strict.php"]);
    Simplicity::includeOnce(Locations::Components, ["exceptional", "exceptions", "parent", "user-deprecated.php"]);
    Simplicity::includeOnce(Locations::Components, ["exceptional", "exceptions", "parent", "user-notice.php"]);
    Simplicity::includeOnce(Locations::Components, ["exceptional", "exceptions", "parent", "user-error.php"]);
    Simplicity::includeOnce(Locations::Components, ["exceptional", "exceptions", "parent", "user-notice.php"]);
    Simplicity::includeOnce(Locations::Components, ["exceptional", "exceptions", "parent", "user-warning.php"]);
    Simplicity::includeOnce(Locations::Components, ["exceptional", "exceptions", "parent", "warning.php"]);
    // Child Exceptions.
    Simplicity::includeOnce(Locations::Components, ["exceptional", "exceptions", "child", "array-to-string-conversion.php"]);
    Simplicity::includeOnce(Locations::Components, ["exceptional", "exceptions", "child", "undefined-variable.php"]);
    Simplicity::includeOnce(Locations::Components, ["exceptional", "exceptions", "child", "file-not-found.php"]);
    Simplicity::includeOnce(Locations::Components, ["exceptional", "exceptions", "child", "include-once.php"]);
    Simplicity::includeOnce(Locations::Components, ["exceptional", "exceptions", "child", "missing-argument.php"]);
    Simplicity::includeOnce(Locations::Components, ["exceptional", "exceptions", "child", "object-to-string-conversion.php"]);
    Simplicity::includeOnce(Locations::Components, ["exceptional", "exceptions", "child", "property-of-none-object.php"]);
    Simplicity::includeOnce(Locations::Components, ["exceptional", "exceptions", "child", "undeclared-static-property.php"]);
    Simplicity::includeOnce(Locations::Components, ["exceptional", "exceptions", "child", "undefined-class.php"]);
    Simplicity::includeOnce(Locations::Components, ["exceptional", "exceptions", "child", "undefined-constant.php"]);
    Simplicity::includeOnce(Locations::Components, ["exceptional", "exceptions", "child", "undefined-index.php"]);
    Simplicity::includeOnce(Locations::Components, ["exceptional", "exceptions", "child", "undefined-offset.php"]);
    Simplicity::includeOnce(Locations::Components, ["exceptional", "exceptions", "child", "undefined-variable.php"]);
    Simplicity::includeOnce(Locations::Components, ["exceptional", "exceptions", "child", "wrong-type.php"]);
    // The supporting Entry object.
    Simplicity::includeOnce(Locations::Components, ["exceptional", "objects", "entry.php"]);

    use Simplicity\Components\Exceptional\Exceptions\{
      ChildException,
      ParentException,
      CustomException
    };
    use Simplicity\Components\Exceptional\Exceptions\Child\{
      ArrayToStringConversion,
      FileNotFound,
      IncludeOnce,
      MissingArgument,
      ObjectToStringConversion,
      PropertyOfNoneObject,
      UndeclaredStaticProperty,
      UndefinedClass,
      UndefinedConstant,
      UndefinedIndex,
      UndefinedOffset,
      UndefinedVariable,
      WrongType
    };
    use Simplicity\Components\Exceptional\Exceptions\Parent\{
      CompileError,
      CompileWarning,
      CoreError,
      CoreWarning,
      Deprecated,
      Error,
      Notice,
      Parse,
      RecoverableError,
      Strict,
      UserDeprecated,
      UserError,
      UserNotice,
      UserWarning,
      Warning
    };
    use Simplicity\Components\Exceptional\Objects\Entry;

    class Registry
    {
      protected static $Instance;

      private $_entries = [];

      public static function getInstance(): Registry
      {
        if(!self::$Instance){
          self::$Instance = new static;
          $instance =& self::$Instance;
          array_map(function(string $exceptionClass) use (&$instance){
            $instance->register($exceptionClass);
          }, self::$Instance->_initialExceptions);
        }
        return self::$Instance;
      }

      public static function register(string $exceptionClassName): Registry
      {
        $instance = self::getInstance();
        if(is_subclass_of($exceptionClassName, ParentException::class)){
          if(!$instance->parentExists($exceptionClassName::exceptionCode())){
            $instance->_entries[$exceptionClassName::exceptionCode()] = new Entry($exceptionClassName);
            return $instance;
          }
        }
        if(is_subclass_of($exceptionClassName, ChildException::class)){
          if($instance->parentExists($exceptionClassName::parentExceptionCode())){
            $instance->get($exceptionClassName::parentExceptionCode())->register($exceptionClassName);
            return $instance;
          }
        }

        if(is_subclass_of($exceptionClassName, ChildException::class)){
          echo "nigger";
        }
        echo "<pre>".print_r($instance, true)."</pre>";
        echo $exceptionClassName."<br />";
        exit();
      }

      public function &get(int $parentCode, int $childCode = null)
      {
        if(is_null($childCode)){
          return $this->_entries[$parentCode];
        }
        return $this->_entries[$parentCode]->getChild($childCode);
      }

      private function parentExists(int $parentKey): bool
      {
        return array_key_exists($parentKey, $this->_entries);
      }

      public function bestMatch(int $code, string $message, string $file, int $line, $context): string
      {
        if (!$this->parentExists($code)) {
          throw new \LogicException("No entry for $code!");
        }
        $entry = $this->get($code);
        if(is_subclass_of($entry->parentClassName(), ParentException::class)) {
          return $entry->bestMatch($message);
        }
        throw new \LogicException("Best match not found!");
      }

      private $_initialExceptions = [
        CompileError::class,
        CompileWarning::class,
        CoreError::class,
        CoreWarning::class,
        Deprecated::class,
        Error::class,
        Notice::class,
        Parse::class,
        RecoverableError::class,
        Strict::class,
        UserDeprecated::class,
        UserError::class,
        UserNotice::class,
        UserWarning::class,
        Warning::class,
        ArrayToStringConversion::class,
        FileNotFound::class,
        IncludeOnce::class,
        MissingArgument::class,
        ObjectToStringConversion::class,
        PropertyOfNoneObject::class,
        UndeclaredStaticProperty::class,
        UndefinedClass::class,
        UndefinedConstant::class,
        UndefinedIndex::class,
        UndefinedOffset::class,
        UndefinedVariable::class,
        WrongType::class
      ];
    }
  }