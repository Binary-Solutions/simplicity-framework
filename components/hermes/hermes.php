<?php

  namespace Simplicity\Components\Hermes
  {
    /** Import the SingletonBasic triat. */
    use Simplicity\Library\Traits\SingletonBasic;
    /** Import the Cache object. */
    use Simplicity\Components\Hermes\Core\Cache;
    /** Import the ModalType Data object.  */
    use Simplicity\Components\Hermes\Data\ModalType;
    /** Import the Hermes Foundation classes. */
    use Simplicity\Components\Hermes\Foundation\{
      DatabaseConfiguration,
      DatabaseConnector,
      DatabaseModal
    };

    /**
     * Class Hermes
     *
     *
     */
    class Hermes
    {
      /** Use the basic singleton object. */
      use SingletonBasic;

      /** @var Cache */
      protected $Cache;

      /**
       * Hermes: constructor
       *
       * This is the SingletonConstructor for hermes.
       * @return Hermes
       */
      public static function _construct(): Hermes
      {
        if(!self::$Instance){
          /** @var Hermes Instance */
          self::$Instance = new static;
          /** @var Cache Cache */
          self::$Instance->Cache = Cache::_construct();
        }
        return self::$Instance;
      }

      /**
       * modal
       *
       * This method will take in the class name of a modal and return the modal ready for use.
       * @param   string        $modalClass     The database modal we are tyring to load.
       * @return  DatabaseModal
       */
      public static function modal(string $modalClass): DatabaseModal
      {
        /** @var Hermes $instance The instance. */
        $instance = self::getInstance();
        /** Route the request to the cache object to handle. */
        return $instance->Cache->get($modalClass);
      }

      /**
       * getConfiguration
       *
       * This will get and return the configuration object for a given modal, when provided with the modal's
       * full class name. It is worth noting that if the configuration object does not already exist in the
       * cache it will be created and added to the cache.
       * @param   string      $modalClass   The class name of the modal to load.
       * @return  DatabaseConfiguration
       */
      public static function getConfiguration(string $configurationClass): DatabaseConfiguration
      {
        /** Pass the request on to the Cache object. */
        return self::getInstance()->Cache->getConfiguration($configurationClass);
      }

      /**
       * getConnector
       *
       * This will get and return an instance of the requested database connector.
       * @param     int     $typeKey    The mo dal type key indicates to us which connector this modal needs.
       * @return    DatabaseConnector
       */
      public static function getConnector(int $typeKey): DatabaseConnector
      {
        /** @var string $connectorClass The string name for this connector class. */
        $connectorClass = ModalType::connectorClass($typeKey);
        /** Get an instance of the connector class and return it. */
        return $connectorClass::getInstance();
      }
    }
  }