<?php

  namespace Simplicity\Components\Hermes\Data
  {
    /** Import all of the type classes. */
    use Simplicity\Components\Hermes\Types\MysqlPdo\{
      MysqlPdoConfiguration,
      MysqlPdoConnector,
      MysqlPdoModal
    };

    /**
     * Class ModalType
     *
     * This is the ModalType data object.
     */
    class ModalType
    {
      /** @const  int     MysqlPDO              The mysql pdo modal type key. */
      public const MysqlPDO = 0;
      /** @const  array   ConfigurationClasses  This is a collection of the configuration classes for all types. */
      private const ConfigurationClasses = [
        self::MysqlPDO => MysqlPdoConfiguration::class
      ];
      /** @const  array   ConnectorClasses      This is a collection of all the Connector classes. */
      private const ConnectorClasses = [
        self::MysqlPDO => MysqlPdoConnector::class
      ];
      /** @const  array   ModalClasses          This is a collection of all the base Modal classes. */
      private const ModalClasses = [
        self::MysqlPDO => MysqlPdoModal::class
      ];

      /**
       * configurationClass
       *
       * This will return the base configuration class name for the modal type key specified.
       *
       * @param int $key
       * @return string
       */
      public static function configurationClass(int $key): string
      {
        return self::ConfigurationClasses[$key];
      }

      /**
       * connectorClass
       *
       * This will return the full class name for the connector that belongs to the modal type specified.
       * @param int $key
       * @return string
       */
      public static function connectorClass(int $key): string
      {
        return self::ConnectorClasses[$key];
      }

      /**
       * modalClass
       *
       * This will get and return a modal base class with its modal type key.
       * @param   int     $key    The type key for the modalClass you need.
       * @return  string  The full modal class name.
       */
      public static function modalClass(int $key): string
      {
        return self::ModalClasses[$key];
      }
    }
  }