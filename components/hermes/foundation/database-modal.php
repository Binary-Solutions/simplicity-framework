<?php

  namespace Simplicity\Components\Hermes\Foundation
  {
    /** Import Hermes. */
    use Simplicity\Components\Hermes\Hermes;

    /**
     * Class DatabaseModal
     *
     * This is the foundation for all modal classes.
     */
    class DatabaseModal
    {
      /** @var  int                       $_modalTypeKey            The modal type key for this modal.  */
      protected $_modalTypeKey;
      /** @var  DatabaseConfiguration     $DatabaseConfiguration    The configuration object.*/
      protected $ConfigurationClass;
      /** @var  DatabaseConnector         $DatabaseConnector        This is the database connector for this modal type. */
      protected $DatabaseConnector;
      /** @var  Object                    $Connection               The database connection. */
      protected $Connection;

      /**
       * DatabaseModal constructor.
       */
      public function __construct()
      {
        /** @var    DatabaseConfiguration     ConfigurationClass    Replace the string Configuration class with the object. */
        $this->ConfigurationClass = Hermes::getConfiguration($this->ConfigurationClass);
        /** @var    DatabaseConnector         DatabaseConnector     This connector will provide the functionality for establishing connections to a pdo database. */
        $this->DatabaseConnector  = Hermes::getConnector($this->_modalTypeKey);
      }

      /**
       * getModalTypeKey
       *
       * This will return the modalTypeKey.
       * @return  int
       */
      public function getModalTypeKey(): int
      {
        return $this->_modalTypeKey;
      }
    }
  }