<?php

  namespace Simplicity\Components\Hermes\Foundation
  {

    /**
     * Class DatabaseConfiguration
     *
     * This is the foundation for all of the database configuration objects.
     */
    class DatabaseConfiguration
    {
      /** @var  int                       $_modalTypeKey            The modal type key for this modal.  */
      protected $_modalTypeKey;

      /**
       * getModalTypeKey
       *
       * This will return the modalTypeKey.
       * @return  int
       */
      public function getModalTypeKey(): int
      {
        return $this->_modalTypeKey;
      }
    }
  }