<?php

  namespace Simplicity\Components\Hermes\Foundation
  {
    /** Import the SingletonBasic class. */
    use Simplicity\Library\Traits\SingletonBasic;

    /**
     * Class DatabaseConnector
     *
     * This is the foundation for all of the database connector objects.
     */
    class DatabaseConnector
    {
      /** Use the basic singleton trait. */
      use SingletonBasic;
      /** @var int  $_modalTypeKey The modal type key for the modal type this belongs to. */
      protected $_modalTypeKey;

      /**
       * getModalTypeKey
       *
       * This will return the modal type key for this specific connector.
       * @return    int     The modal type key.
       */
      public function getModalTypeKey(): int
      {
        return $this->_modalTypeKey;
      }
    }
  }