<?php

  namespace Simplicity\Components\Hermes\Exceptions
  {

    class MissingConfigurationPropertyException extends HermesException
    {
      protected const ExceptionCode = 1;

      protected const MessageString = "Database configuration missing property: %s";

      public function __construct(string $propertyName, string $file = __FILE__, int $line = __LINE__){
        parent::__construct(
          sprintf(self::MessageString, $propertyName),
          $file,
          $line,
          null
        );
      }
    }
  }