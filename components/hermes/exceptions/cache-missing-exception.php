<?php

  namespace Simplicity\Components\Hermes\Exceptions
  {

    use Simplicity\Components\Exceptional\Exceptions\SimpleException;

    class CacheMissingException extends HermesException
    {
      protected const ExceptionCode = 2;

      protected const MessageString = "The %s %s missing from the cache.";

      public function __construct(string $type, string $name, string $file = __FILE__, int $line = __LINE__){
        parent::__construct(
          sprintf(self::MessageString, $type, $name),
          $file,
          $line
        );
      }
    }
  }