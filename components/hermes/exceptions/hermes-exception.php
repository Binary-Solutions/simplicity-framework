<?php

  namespace Simplicity\Components\Hermes\Exceptions
  {

    use Simplicity\Components\Exceptional\Exceptions\ComponentException;
    use Simplicity\Components\Exceptional\Exceptions\SimpleException;

    class HermesException extends ComponentException
    {
      protected const ComponentName = "Hermes";

      protected const ComponentCode = 4;

      public function __construct(
        string $message = "",
        string $file = __FILE__,
        int $line = __LINE__,
        ?SimpleException $previous
      ){
        parent::__construct($message, $file, $line, $previous);
      }
    }
  }