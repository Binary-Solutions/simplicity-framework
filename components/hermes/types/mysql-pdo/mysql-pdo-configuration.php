<?php

  namespace Simplicity\Components\Hermes\Types\MysqlPdo
  {
    /** Import the ModalType data class. */
    use Simplicity\Components\Hermes\Data\ModalType;
    /** Import the DatabaseConfiguration class. */
    use Simplicity\Components\Hermes\Foundation\DatabaseConfiguration;

    /**
     * Class MysqlPdoConfiguration
     *
     * This is the base for the a Mysql PDO configuration object.
     */
    class MysqlPdoConfiguration extends DatabaseConfiguration
    {
      /** @const  array     DefaultAttributes     This is a collection of default of failsafe attributes for connection.*/
      protected const DefaultAttributes = [
        [\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION],
        [\PDO::ATTR_EMULATE_PREPARES, false]
      ];
      /** @var    int                           $_modalTypeKey        This is the Modal type key. It tells the fra */
      protected $_modalTypeKey = ModalType::MysqlPDO;
      /** @var    string      $host                 The database host. */
      protected $host;
      /** @var    int         $port                 The database port. */
      protected $port;
      /** @var    array|null  $attributes           This will be an array of connection attributes or null if none were set. */
      protected $attributes;
      /** @var    string      $databaseName         The database name. */
      protected $databaseName;
      /** @var    string      $username             The connection username. */
      protected $username;
      /** @var    string      $password             The connection password. */
      protected $password;

      /**
       * getHost
       *
       * This will return the host name.
       * @return string host name
       */
      public function getHost(): string
      {
        return $this->host;
      }

      /**
       * getPort
       *
       * This will get the host port
       * @return int  The host port.
       */
      public function getPort(): int
      {
        return $this->port;
      }

      /**
       * getDatabaseName
       *
       * This will get and return the database name.
       * @return  string   The database name.
       */
      public function getDatabaseName(): string
      {
        return $this->databaseName;
      }

      /**
       * getUsername
       *
       * This will get and return the username.
       * @return string
       */
      public function getUsername(): string
      {
        return $this->username;
      }

      /**
       * getPassword
       *
       * This will return the password string.
       * @return    string    The password string.
       */
      public function getPassword(): string
      {
        return $this->password;
      }

      /**
       * getAttributes
       *
       * This will get and return the attributes for this connection. It should be noted
       * that if none are provided the object does have a set of fall back attributes.
       * @return array
       */
      public function getAttributes(): array
      {
        if(!$this->attributes){
          return self::DefaultAttributes;
        }
        return $this->attributes;
      }
    }
  }