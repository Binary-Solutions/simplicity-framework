<?php

  namespace Simplicity\Components\Hermes\Types\MysqlPdo
  {
    /** Import the ModalType data object. */
    use Simplicity\Components\Hermes\Data\ModalType;
    /** Import the DatabaseConnector class. */
    use Simplicity\Components\Hermes\Foundation\DatabaseConnector;

    /**
     * Class MysqlPdoConnector
     *
     * This is the Core for the MysqlPdo modals this object extends the base connector and provides the
     * modal with the ability to establish and maintain a connection to a pdo database.
     */
    class MysqlPdoConnector extends DatabaseConnector
    {
      /** @var    int     $_modalTypeKey    This is the modal type key this connector belongs to.  */
      protected $_modalTypeKey = ModalType::MysqlPDO;

      /**
       * openConnection
       *
       * This method takes in a MysqlPdoConfiguration object and will return a usable
       * \PDO Connection object.
       * @param     MysqlPdoModal     $modal          The modal we are establishing a connection for.
       * @return    \PDO              The connection.
       */
      public function openConnection(MysqlPdoModal $modal): \PDO
      {
        /** @var MysqlPdoConfiguration $configuration Get the configuration object. */
        $configuration = $modal->getConfiguration();
        /** @var \PDO $Connection Construct the new connection. */
        $Connection = new \PDO(
          "mysql" .
          ':host=' . $configuration->getHost() .
          ';port=' . $configuration->getPort() .
          ';dbname=' . $configuration->getDatabaseName(),
          $configuration->getUsername(),
          $configuration->getPassword()
        );
        /** Set the connection attributes. */
        $this->setAttributes($Connection, $configuration->getAttributes());
        /** Return the \PDO Connection object. */
        return $Connection;
      }

      /**
       * closeConnection
       *
       * Pass-through method for close connection just in case of mistakes.
       * @param MysqlPdoModal $modal
       */
      public function closeConnection(MysqlPdoModal $modal)
      {
        /** Pass the request off to the modal. */
        $modal->closeConnection();
      }

      /**
       * setAttributes
       *
       * This method will take in the active connection object and an array of connection
       * attributes to set them and apply all of them for you.
       * @param   \PDO    $connection   The database connection.
       * @param   array   $attributes   The array of attributes to apply to the connection.
       */
      protected function setAttributes(\PDO $connection, array $attributes): void
      {
        foreach($attributes as $attribute){
          list($argumentOne, $argumentTwo) = $attribute;
          $connection->setAttribute($argumentOne, $argumentTwo);
        }
      }
    }
  }