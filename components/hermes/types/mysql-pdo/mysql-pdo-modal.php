<?php

  namespace Simplicity\Components\Hermes\Types\MysqlPdo
  {
    /** Import the ModuleType data object. */
    use Simplicity\Components\Hermes\Data\ModalType;
    /** Import the DatabaseModal class. */
    use Simplicity\Components\Hermes\Foundation\DatabaseModal;

    /**
     * Class MysqlPdoModal
     *
     * The is the Core for the all the MysqlPdoModals. This will do all of the leg work needed
     * for getting a Mysql PDO Modal setup and ready to use.
     */
    class MysqlPdoModal extends DatabaseModal
    {
      /** @var    int                           $_modalTypeKey        This is the Modal type key. It tells the fra */
      protected $_modalTypeKey = ModalType::MysqlPDO;
      /** @var    MysqlPdoConfiguration         $ConfigurationClass   The Configuration object for this modal type. */
      protected $ConfigurationClass;
      /** @var    MysqlPdoConnector             $DatabaseConnector    This will be the database connector for this modal type.  */
      protected $DatabaseConnector;
      /** @var    null|\PDO                     $Connection           This is the database connection provided by the Connector. */
      protected $Connection;

      /**
       * MysqlPdoModal constructor.
       *
       * This is the Mysql PDO Modal construct.
       */
      public function __construct()
      {
        parent::__construct();
        /** Open the database connection. */
        $this->openConnection();
      }

      // Query methods.

      /**
       * execute
       *
       * Execute the prepared statement.
       * @param   \PDOStatement   $statement      The statement to execute.
       * @return  \PDOStatement   The result of executing the statement.
       */
      protected function execute(\PDOStatement $statement): \PDOStatement
      {
        try {
          /** Try to just execute the statement. */
          $statement->execute();
        } catch (\PDOException $exception){
          /** Check if this is a code 2006 or stale connection. */
          if($exception->getCode() != 2006){
            /** Not a stale connection rethrow it. */
            throw $exception;
          }
          /** Stale connection reconnect. */
          $this->resetConnection();
          /** Re-try to execute the statement. */
          $this->execute($statement);
        }
        /** Return the statement. */
        return $statement;
      }

      /**
       * prepare
       *
       * This will prepare a pdo prepared statement type query.
       * @param     string    $query    The query we want to run.
       * @return    \PDOStatement
       */
      protected function prepare(string $query): \PDOStatement
      {
        try{
          /** Prepare the statement. */
          return $this->Connection->prepare($query);
        } catch (\PDOException $exception){
          echo "<pre>".print_r($query, true)."</pre>";
        }
      }

      /**
       * initializeTransaction
       *
       * Initialize a mysql transaction so we can choose to roll the changes back if needed.
       */
      protected function initializeTransaction(): void
      {
        /** Pass the request onb to the connection object. */
        $this->Connection->beginTransaction();
      }

      /**
       * rollbackTransaction
       *
       * Rollback all the changes made in a transaction.
       */
      protected function rollbackTransaction(): void
      {
        /** Pass the request along to the Connection object. */
        $this->Connection->rollBack();
      }

      /**
       * commitTransaction
       *
       * Commit all of the changes made in a transaction.
       */
      protected function commitTransaction(): void
      {
        /** Pass the request along to the Connection object. */
        $this->Connection->commit();
      }

      // Connection methods

      /**
       * openConnection
       *
       * This will open the database connection and set the Connection property.
       */
      public function openConnection(): void
      {
        /** Make sure we dont already have a connection. */
        if(!$this->Connection){
          $this->Connection = $this->DatabaseConnector->openConnection($this);
        }
      }

      /**
       * closeConnection
       *
       * This will close the database connection.
       */
      public function closeConnection(): void
      {
        /** Unset the connection var destroying the object and closing the connection. */
        unset($this->Connection);
      }

      /**
       * resetConnection
       *
       * This will reset the connection and provide you with a fresh one.
       */
      public function resetConnection(): void
      {
        /** First close the old connection. */
        $this->closeConnection();
        /** Then open a new connection in its place. */
        $this->openConnection();
      }

      // Getter methods

      /**
       * databaseName
       *
       * This will return the database name we should be connected to.
       * @return string
       */
      protected function databaseName(): string
      {
        /** Pass the request on to the Configuration object. */
        return $this->ConfigurationClass->getDatabaseName();
      }

      /**
       * getConfiguration
       *
       * This will return the configuration object.
       * @return  MysqlPdoConfiguration
       */
      public function getConfiguration(): MysqlPdoConfiguration
      {
        /** Return the MysqlPdoConfiguration object. */
        return $this->ConfigurationClass;
      }

      /**
       * lastInsertId
       *
       * This will return the lastInsertId
       * @return int|null
       */
      protected function lastInsertId(): ?int
      {
        /** Return the lastInsertId from the connection object. */
        return $this->Connection->lastInsertId();
      }
    }
  }