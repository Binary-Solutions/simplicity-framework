<?php

  namespace Simplicity\Components\Hermes\Core
  {
    /** Import the SingletonBasic trait. */
    use Simplicity\Library\Traits\SingletonBasic;
    /** Import the UndefinedIndex Exception class. */
    use Simplicity\Components\Exceptional\Exceptions\Child\UndefinedIndex;

    /**
     * Class ModalCache
     *
     * This is the ModalCache it keeps tradck of all the Modals that have been created.
     */
    class ModalCache
    {
      /** use the SingletonBasic trait. */
      use SingletonBasic;
      /** @var array  */
      private $_modals = [];

      /**
       * add
       *
       * This get method is acting ass an add and a get for the time being.
       * I just needed a basic version of the component working.
       * @param   string  $modalName  The class name of the modal needed.
       * @return  mixed
       */
      public function get(string $modalName)
      {
        try {
          return $this->_modals[$modalName];
        } catch (UndefinedIndex $exception){
          $this->_modals[$modalName] = new $modalName();
          return $this->_modals[$modalName];
        }
      }
    }
  }