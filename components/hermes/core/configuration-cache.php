<?php

  namespace Simplicity\Components\Hermes\Core
  {
    /** Improt the Singleton basic trait. */
    use Simplicity\Library\Traits\SingletonBasic;
    /** Import the SimpleException class. */
    use Simplicity\Components\Exceptional\Exceptions\SimpleException;
    /** Import the DatabaseConfiguration class. */
    use Simplicity\Components\Hermes\Foundation\DatabaseConfiguration;

    /**
     * Class ConfigurationCache
     *
     * This is the ConfigurationCache it keeps track of all the configuration objects just in
     * case we can reuse one.
     */
    class ConfigurationCache
    {
      /** use the Singleton basic trait. */
      use SingletonBasic;
      /** @var array  */
      private $_configurations = [];

      /**
       * get
       *
       * This will get the confuration object if it is in the collection you specifdied.
       * if it is not it is passed to add to create it then returned.
       * @param   string    $selector   The class name for the configuration object needed.
       * @return  DatabaseConfiguration
       */
      public function get(string $selector): DatabaseConfiguration
      {
        try {
          /** Assume its there and return it. */
          return $this->_configurations[$selector];
        } catch (SimpleException $exception){
          /** It was not there so add it and return it. */
          return $this->add($selector);
        }
      }

      /**
       * add
       *
       * This will add the Configuration object to the collection with the class name provided.
       * @param string $className
       * @return DatabaseConfiguration
       */
      private function &add(string $className): DatabaseConfiguration
      {
        /** Add it to the array */
        $this->_configurations[$className] = new $className();
        /** @var DatabaseConfiguration $reference */
        $reference =& $this->_configurations[$className];
        /** Return the reference. */
        return $reference;
      }
    }
  }