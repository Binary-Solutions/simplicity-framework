<?php

  namespace Simplicity\Components\Hermes\Core
  {
    /** Import the SingletonBasic trait. */
    use Simplicity\Library\Traits\SingletonBasic;
    /** Import the DatabaseConfiguration class. */
    use Simplicity\Components\Hermes\Foundation\DatabaseConfiguration;

    /**
     * Class Cache
     *
     * The Cache class is in charge of keeping track of all the created modals so we
     * dont end up creating them multiple times each and creating work for ourselves.
     */
    class Cache
    {
      /** The Singleton Basic trait. */
      use SingletonBasic;

      /** @var ConfigurationCache */
      protected $ConfigurationCache;
      /** @var ConfigurationCache */
      protected $ConnectionCache;
      /** @var ModalCache */
      protected $ModalCache;

      /**
       * Cache: constructor.
       *
       * This is the Singleton constructor for the Cache object.
       * @return  Cache
       */
      public static function _construct(): Cache
        {
        if(!self::$Instance){
          /** @var Cache Instance */
          self::$Instance = new static;
          /** @var ConfigurationCache ConfigurationCache */
          self::$Instance->ConfigurationCache = ConfigurationCache::getInstance();
          /** @var ModalCache ModalCache */
          self::$Instance->ModalCache         = ModalCache::getInstance();
        }
        return self::$Instance;
      }

      /**
       * get
       *
       * This is a pass-through method to the get method
       * @param string $modalClass
       * @return mixed
       */
      public function get(string $modalClass)
      {
        /** Pass the requyest to the ModalCache. */
        return $this->ModalCache->get($modalClass);
      }

      /**
       * getConfiguration
       *
       * This will get the requested Configuration object.
       * @param   string    $className    The configuration class name.
       * @return  DatabaseConfiguration
       */
      public function &getConfiguration(string $className): DatabaseConfiguration
      {
        /** @var DatabaseConfiguration $configuration */
        $configuration = $this->ConfigurationCache->get($className);
        /** Return the configuration object. */
        return $configuration;
      }
    }
  }