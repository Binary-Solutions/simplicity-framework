<?php

  namespace Simplicity
  {
    /** Include the SingletonBasic trait. */
    include_once("library" . DIRECTORY_SEPARATOR . "traits" . DIRECTORY_SEPARATOR . "basic-singleton.php");
    /** Include the AutoConfigure  object for the framework. */
    include_once("core".DIRECTORY_SEPARATOR."auto-configure.php");

    use Simplicity\Components\{
      Exceptional\Exceptions\Child\IncludeOnce,
    };


    use Simplicity\Core\Flags;
    use Simplicity\Core\Locations;
    use Simplicity\Core\AutoConfigure;
    use Simplicity\Core\EventRegistry;
    use Simplicity\Core\Foundation\FrameworkApplication;
    use Simplicity\Library\Traits\BasicSingleton;

    /**
     * Class Simplicity
     *
     * This is the interface for the Simplicity framework.
     */
    class Simplicity
    {
      /** Use the SingletonBasic trait. */
      use BasicSingleton;
      /** @var      Flags                   $Flags              This is a reference to the flags object it keeps track of all the boolean switches associated with Simplicity. */
      private $Flags;
      /** @var      EventRegistry           $EventRegistry      This handles all of the functionality for dispatching the events the framework handles. */
      private $EventRegistry;
      /** @var      FrameworkApplication    $Application        The application object currently running. */
      private $Application;

      /**
       * Simplicity static constructor.
       *
       * @param null|FrameworkApplication $application
       * @return Simplicity
       */
      public static function _construct(?FrameworkApplication $application = null): Simplicity
      {
        if(!self::$Instance){
          try {
            /** @var Simplicity Instance  Replace the empty static variable with the new Simplicity instance. */
            self::$Instance = new static;
            /** Execute the auto configure program it will take care of the rest of the framework setup. */
            AutoConfigure::execute($application);



          } catch (\Throwable|\Exception|\ErrorException|\TypeError $exception){
            // THIS IS WHERE I SHOULD HANDLE FATAL ERRORS CONSTRUCT.

          }
        }
        return self::$Instance;
      }

      /**
       * run
       *
       * This method is where the framework gives up primary control to the application.
       */
      private function run(): void
      {
        /**
         * From now on the application should be wrapped in this catch statement unless you are
         * doing some wacky php async process.
         */
        try {
          /** Before we hand over control make sure to execute any startup tasks that were provided. */
          $this->EventRegistry->execute(EventRegistry::StartUp);



        } catch (\Throwable $exception){
          echo "<pre>".print_r($exception, true)."</pre>";
        }
      }

      /**
       * handleException
       *
       * This is the handle exception method. Any exception that is thrown in the framework or an application wrapped in
       * in it comes here if nothing catch's it. This is the end of the line. This method will check for handlers for
       * the type of exception it has then check if the application wants to handle the exception lastly it will log
       * the exception itself so that there is record of exactly what happen.
       * @param   \Error|\ErrorException|\Exception|\Throwable|\TypeError   $exception
       * @todo    Finish this.
       */
      public static function handleException($exception): void
      {
        echo "<pre>".print_r($exception, true)."</pre>";
      }

      /**
       * handleShutdown
       *
       * This is the handle shutdown method. This is one of the more misunderstood methods in php. Most seem to think
       * that a shutdown handler is only for when something goes wrong. But in fact the shutdown handler is called
       * every time a page is loaded or a script is done executing. By using this method we can save thing for after
       * the script is done, then close the connection to the client and do the work without them needing to wait
       * on a loading wheel that really should not be stopping them.
       * @return  void
       * @todo    finish this method.
       */
      public static function handleShutdown(): void
      {

        $lastError = error_get_last();

        if(!is_null($lastError)){
          list($type, $message, $file, $line) = $lastError;
          echo sprintf(
            "<h1>Fatal Error</h1>
                   <string>Message: </string>%s<br />
                   <string>File: </string>%s<br />
                   <strong>Line: </strong>%s",
            $message,
            $file,
            $line
          );
        }

        /** @var Simplicity $instance   This is an instance of Simplicity. */
        $instance = self::getInstance();


        /** Execute all of the shutdown functions that have been registered. */
        $instance->EventRegistry->execute(EventRegistry::Shutdown);
      }

      /**
       * is
       *
       * This is another pass through method. It takes the provided selector passes it to the flags object and returns the result.
       * @param   int   $selector   One of the flag selectors defined in the flags object
       * @return  bool
       */
      public static function is(int $selector): bool
      {
        /** Pass the selector down to the Flags object and return the result. */
        return self::getInstance()->Flags->is($selector);
      }

      /**
       * includeOnce
       *
       * This is the include once method wrapper.
       * @param   string|int      $location     This can be a location string or a location int key.
       * @param   array|null      $path         The path array to use with a location int key or null.
       * @throws  IncludeOnce
       */
      public static function includeOnce($location, ?array $path = null): void
      {
        /** Check if the path is null. */
        if(!is_null($path)){
          /** @var string $location Get the base path string from the locations object. */
          $location = Locations::get($location);
          /** @var array $part iterate over the parts collection. */
          foreach ($path as $part){
            /** Append each parth to the location string. */
            $location .= DIRECTORY_SEPARATOR . $part;
          }
        }
        /** Try to include the file. */
        include_once $location;
      }

      /**
       * setFlag
       *
       * This is a simple router method it takes the setFlags method and pipes the paramaters to the Flags::set method.
       * @param   int   $selector   The flag selector to set.
       * @param   bool  $value      The value to set the flag to.
       */
      public static function setFlag(int $selector, bool $value): void
      {
        /** Pass the request to set the flag to the flags object. */
        self::getInstance()->Flags->set($selector, $value);
      }

      /**
       * setFlags
       *
       * This will set the reference to the flags object in Simplicity.
       * @param   Flags   $flags    The instance of the flags object.
       * @return  void
       */
      public static function setFlags(Flags $flags): void
      {
        /** @var Flags Flags  Save a reference to the flags object. */
        self::getInstance()->Flags = $flags;
      }

      /**
       * setEventRegistry
       *
       * This will take the event registry object and set it to the event registry for the Simplicity.
       * @param   EventRegistry   $eventRegistry    The event registry that will manage all of the start up and shut down events.
       * @return  void
       */
      public static function setEventRegistry(EventRegistry $eventRegistry): void
      {
        /** @var EventRegistry EventRegistry */
        self::getInstance()->EventRegistry = $eventRegistry;
      }

      /**
       * setApplication
       *
       * Set the application reference in Simplicity.
       * @param FrameworkApplication $application
       */
      public static function setApplication(FrameworkApplication $application): void
      {
        /** @var FrameworkApplication Application The application currently running. */
        self::getInstance()->Application = $application;
      }

      /**
       * isConfigured
       *
       * This method will return if the framework has been configured yet or not.
       * @return bool
       */
      public function isConfigured(): bool
      {
        /** Return the value of the configured flag. */
        return $this->_configured;
      }
    }

    /** Simplicity is constructed the moment it is included this way you dont have to worry about if it is ready or waiting. */
    Simplicity::_construct();
  }